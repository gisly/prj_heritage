# coding=utf-8
__author__ = 'gisly'
import codecs, os

from src.ru.gisly.CorpusDb import CorpusDb

LINE_TO_START = 63
LINE_END = '</body>'

corpusDb = CorpusDb()

def parseSurnameFolder(foldername):
    for filename in os.listdir(foldername):
        parseSurnameFile(os.path.join(foldername, filename))
        print filename
        
        
def parseSurnameFile(filename):
    with codecs.open(filename, 'r', 'cp1251') as fin:
        for i, line in enumerate(fin):
            if LINE_END in line:
                break
            if i>=LINE_TO_START:
                surname = line.strip()
                if surname:
                    corpusDb.insertSurname({'surname':surname})
                
                
                
parseSurnameFolder("D:\\LingM\\Dictionaries\\surnames\\surnames")
            