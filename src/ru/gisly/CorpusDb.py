# coding=utf-8
__author__ = 'gisly'

import pymongo, os, sys, re

from datetime import datetime






class CorpusDb(object):
    def __init__(self,host='localhost',port=27017,dbName='googleNGRAMS'):
        
        self.connection=pymongo.Connection(host,port)
        self.db=self.connection[dbName]


    def __del__(self):
        print 'dying'
        self.connection.disconnect()

    def insertNGram(self, ngram):
        try:
            
            colNGrams=self.db.nGrams
            ngramFound = colNGrams.find({'ngram0':ngram['ngram0'], 'ngram1':ngram['ngram1'], 'year':ngram['year']})
            isFound = False
            for res in ngramFound:
                isFound = True
                break
            if not isFound:
                print 'not found:'+ngram['ngram0']
            #TODO:
            #colNGrams.insert(ngram)
        except Exception, e:
            print(str(e))


    def insertFilteredNGram(self, ngram):
        try:
            colNGrams=self.db.filteredGoogleNGrams
            ngramFound = colNGrams.find({'ngram0':ngram['ngram0'], 'ngram1':ngram['ngram1'], 'year':ngram['year']})
            isFound = False
            for res in ngramFound:
                isFound = True
                break
            if not isFound:
                colNGrams.insert(ngram)
            
            #colNGrams.insert(ngram)
            #colNGrams.update(ngram, {'upsert':True})
        except Exception, e:
            print(str(e))
            
            
    def removeDuplicates(self, ngram):
        try:
            colNGrams=self.db.filteredGoogleNGrams
            ngramFound = colNGrams.find({'ngram0':ngram['ngram0'], 'ngram1':ngram['ngram1'], 'year':ngram['year']})
            isFound = False
            for res in ngramFound:
                isFound = True
                break
            if isFound:
                print "duplicate"+ngram['ngram0'] +'_' + ngram['ngram1']
                colNGrams.remove({'ngram0':ngram['ngram0'], 'ngram1':ngram['ngram1'], 'year':ngram['year']})
                colNGrams.insert(ngram)
        except Exception, e:
            print(str(e))

    def insertFilteredTriGram(self, ngram):
        try:
            colNGrams=self.db.filteredGoogleTriGrams
            colNGrams.insert(ngram)
            #colNGrams.update(ngram, {'upsert':True})
        except Exception, e:
            print(str(e))


    def getFilteredNGram(self, ngram0, ngram1, year):
        return self.db.filteredGoogleNGrams.find({'ngram0':ngram0,'ngram1':ngram1, 'year':year})
    def getFilteredNGramAnyYear(self, ngram0, ngram1):
        return self.db.filteredGoogleNGrams.find({'ngram0':ngram0,'ngram1':ngram1})

    def removeFilteredNGram(self, ngram):
        self.db.filteredGoogleNGrams.remove(ngram)

    def insertRCBiGram(self, ngram):
        try:
            colRCNBigrams=self.db.rcBigrams
            colRCNBigrams.insert(ngram)
        except Exception, e:
            print(str(e))

    def insertRCTriGram(self, ngram):
        try:
            colRCNTrigrams=self.db.rcTrigrams
            colRCNTrigrams.insert(ngram)
        except Exception, e:
            print(str(e))

    def insertWikibigram(self, word0, word1, filename):
        try:
            colWikibigrams=self.db.wikiBigrams
            dataFound=colWikibigrams.find({'ngram0':word0, 'ngram1':word1})
            flag=False
            for item in dataFound:
                flag=True
                colWikibigrams.remove(item)
                colWikibigrams.insert({'ngram0':word0, 'ngram1':word1,'count':item['count']+1, 
                                       'upddate':str(datetime.now()), 'filename':filename})
                break
            if not flag:
                colWikibigrams.insert({'ngram0':word0, 'ngram1':word1,'count':1, 
                                       'upddate':str(datetime.now()), 'filename':filename})
        except Exception, e:
            print(str(e))
            
            
    def insertWikibigramDict(self, bigram, filename):
        try:
            colWikibigrams=self.db.tempWikiBigrams
            dataFound=colWikibigrams.find({'ngram0':bigram['ngram0'], 'ngram1':bigram['ngram1']})
            flag=False
            for item in dataFound:
                flag=True
                colWikibigrams.remove(item)
                colWikibigrams.insert({'ngram0':bigram['ngram0'], 'ngram1':bigram['ngram1'],
                                       'count':item['count']+bigram['count'], 
                                       'upddate':str(datetime.now()), 'filename':filename})
                break
            if not flag:
                colWikibigrams.insert({'ngram0':bigram['ngram0'], 'ngram1':bigram['ngram1'],'count':bigram['count'], 
                                       'upddate':str(datetime.now()), 'filename':filename})
        except Exception, e:
            print(str(e))


    def insertEvent(self, event):
        colEvents=self.db.events
        colEvents.insert(event)


    def insertPrepositionCase(self, preposition, case):
        colPreps=self.db.prepositions
        prepData=colPreps.find({'prep':preposition, 'case':case})
        flag=False
        for item in prepData:
            flag=True
            colPreps.remove(item)
            colPreps.insert({'prep':preposition, 'case':case,'count':item['count']+1})
        if not flag:
            colPreps.insert({'prep':preposition, 'case':case, 'count':1})


    def insertOperation(self, operation):
        colOperations=self.db.operations
        opData=colOperations.find(operation)
        flag=False
        for item in opData:
            flag=True
            operation['count']=item['count']+1
            colOperations.remove(item)
            colOperations.insert(operation)
        if not flag:
            operation['count']=1
            colOperations.insert(operation)
            
            
    def insertSurname(self, surname):
        self.db.surnames.insert(surname)
        
    def existsSurname(self, surname):
        return self.db.surnames.find(surname).count() > 0
    
    def insertName(self, name):
        self.db.names.insert(name)
        
    def existsName(self, name):
        return self.db.names.find(name).count() > 0
    
    def insertGeo(self, geo):
        self.db.geos.insert(geo)
        
    def existsGeo(self, geo):
        return self.db.geos.find(geo).count() > 0



    def groupRCCorpus(self):
        colRCNGrams=self.db.rcNGrams
        colRCNGramsSorted=self.db.rcNGrams.find({'grouped':None}).sort({'ngram0':1,'ngram1':1})
        prevWord0=None
        prevWord1=None
        totalCount=0
        for ngram in colRCNGramsSorted:
            if prevWord0 is not None:
                if prevWord0!=ngram['word0'] and  prevWord1!=ngram['word1'] :
                    newNgram=dict()
                    newNgram['word0']=prevWord0
                    newNgram['word1']=prevWord1
                    newNgram['match_count']=totalCount
                    newNgram['grouped']='yes'
                    colRCNGrams.insert(newNgram)
                else:
                    totalCount+=ngram['match_count']
            prevWord0 =ngram['word0']
            prevWord1 =ngram['word1']


    #get a list of words which occur on the right of 'word' in al bigrams
    def getRightNeighbours(self, word):
        colNGrams=self.db.filteredGoogleNGrams
        rightNeighbours=[]
        colNGramsFound=colNGrams.find({'ngram0':word})
        for colNGram in colNGramsFound:
            word2Index='ngram1'
            if word2Index in colNGram:
                word2 = colNGram[word2Index]
                if not word2 in rightNeighbours:
                    rightNeighbours.append(word2)
        return rightNeighbours



        #get a list of words which occur on the right of 'word' in al bigrams
    def getRCRightNeighbours(self, word):
        colNGrams=self.db.filteredGoogleNGrams
        rightNeighbours=[]
        colNGramsFound=colNGrams.find({'ngram0':word})
        for colNGram in colNGramsFound:
            word2Index='ngram1'
            if word2Index in colNGram:
                word2 = colNGram[word2Index]
                if not word2 in rightNeighbours:
                    rightNeighbours.append(word2)
        return rightNeighbours

    # sum the match_counts of all the bigrams where word1 is on the left of word2
    def getScoreNGram(self, word1, word2):
        colNGrams=self.db.filteredGoogleNGrams
        ngrams0=colNGrams.find({'ngram0':word1, 'ngram1':word2, 'year':0})
        ngrams1=colNGrams.find({'ngram0':word1, 'ngram1':word2, 'year':1})
        res0 = 0
        for res in ngrams0:
            res0 = res['match_count']
            break

        res1 = 0
        for res in ngrams1:
            res1 = res['match_count']
            break

        return res0 + res1
        #ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2}).limit(2)
        #return self.getMatchCount(ngrams)
        #return sum([matchCount for matchCount in (ngram['match_count'] for ngram in ngrams)])


    # sum the match_counts of all the bigrams where word1 is on the left of word2
    def getRCScoreBiGram(self, word1, word2):
        colNGrams=self.db.rcBigrams
        ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2})
        #ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2}).limit(1)
        return self.getMatchCount(ngrams)
        #return sum([matchCount for matchCount in (ngram['match_count'] for ngram in ngrams)])

    def getRCScoreTriGram(self, word1, word2, word3):
        colNGrams=self.db.rcTrigrams
        ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2, 'ngram2':word3})
        #ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2, 'ngram2':word3}).limit(2)
        return self.getMatchCount(ngrams)
        #return sum([matchCount for matchCount in (ngram['match_count'] for ngram in ngrams)])

    def getWikiScoreBiGram(self, word1, word2):
        colNGrams=self.db.wikiBigrams
        ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2})
        return self.getMatchCount(ngrams, 'count')
    
    def getRCScoreMorphBigram(self, word1, word2):
        colNGrams=self.db.morphNgrams
        word1Reg = re.compile("^"+word1+".*")
        word2Reg = re.compile("^"+word2+".*")
        ngrams=colNGrams.find({'ngram0':word1Reg, 'ngram1':word2Reg})
        #ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2}).limit(1)
        return self.getMatchCount(ngrams)
    
    def getRCScoreMorphProbBigram(self, word1, word2):
        colNGrams=self.db.morphProbBigrams
        ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2})
        return self.getMatchCount(ngrams, countWord ='logProb', initResult = -200)

    def getMatchCount(self, ngrams, countWord = 'match_count', initResult=0):
        result = initResult
        for ngram in ngrams:
            result = ngram[countWord]
            break
        return result
    
    def countUnigram(self, word):
        colNGrams=self.db.filteredGoogleNGrams
        return colNGrams.find({'ngram0':word}).count() + colNGrams.find({'ngram1':word}).count()

    def release(self):
        print 'releasing connection'
        self.connection.disconnect()
