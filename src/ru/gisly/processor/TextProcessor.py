# coding=utf-8
__author__ = 'gisly'
import os
import codecs
import tkMessageBox
from Tkconstants import END
import Tkinter
import tkFileDialog
from src.ru.gisly.syntaxChecking.checkGoverning import MorphoVerifier
from src.ru.gisly.lemmer import Tokenizer
from src.ru.gisly.spellchecking.spellchecking import getSuggestions, isCorrect



class TextProcessor(object):

    tokenizer= Tokenizer()
    morphoVerifier = MorphoVerifier()

    PUNCT_MARKS = [',','.', '...','--','-','!', '?', '<', '>','\'\'','``', u'«', u'»', '(', ')', ':', ';']
    BAD_SYMBOLS = ['<','>','&']
    NUM_OF_CHANGES = 2
    #RUS_CORPORA='rc'

    ENTRY_WIDTH=100


    ###HIGHLIGHTING STYLE####
    SPELLING_ERROR = 'spelling'
    AGR_GOV_ERROR = 'agr_gov'

    SPELLING_BACKGROUND = 'orange'
    AGR_GOV_BACKGROUND = 'pink'

    ############FILES###############

    curFileName = None
    curBeg = None

    TEMP_FILENAME='./tempFile.txt'
    RESULT_FOLDER = os.path.dirname(os.path.abspath(__file__))+'\\results\\'




    def __init__(self):
        self.startVisualizer()



    def startVisualizer(self):
        self.root=Tkinter.Tk()
        self.addCheckControls()
        self.addFileControls()





        self.textWidget = Tkinter.Text(self.root,width=self.ENTRY_WIDTH,
            font=("Times", 14, "bold"))
        self.textWidget.pack(side="bottom")
        self.textWidget.tag_config(self.SPELLING_ERROR, background=self.SPELLING_BACKGROUND)

        self.textWidget.tag_config(self.AGR_GOV_ERROR, background=self.AGR_GOV_BACKGROUND)
        self.textWidget.tag_bind(self.SPELLING_ERROR, "<Double-Button-1>", self.on_spelling_error)
        self.textWidget.tag_bind(self.SPELLING_ERROR, "<Button-1>", lambda x: self.clearListbox())
        self.textWidget.tag_bind(self.SPELLING_ERROR, "<KeyPress>", self.on_edit_spelling_error)
        self.textWidget.tag_bind(self.SPELLING_ERROR, "<Key>", self.on_edit_spelling_error)
        self.textWidget.tag_bind(self.SPELLING_ERROR, "<BackSpace>", self.on_edit_spelling_error)


        self.root.mainloop()

    ####VISUALIZER#####

    def addCheckControls(self):
        checkPanel = Tkinter.Canvas(self.root)
        checkPanel.pack(side="right")

        spellingButton=Tkinter.Button(checkPanel, text= "check spelling",
            command=self.onSpelling)
        spellingButton.pack(side="top")
        governingButton=Tkinter.Button(checkPanel, text= "check government and agreement",
            command=self.onGoverning)
        governingButton.pack(side="bottom")
        self.spellcheckingListbox = Tkinter.Listbox(checkPanel)
        self.spellcheckingListbox.pack(side="left")
        self.spellcheckingListbox.bind("<<ListboxSelect>>", self.on_choose_replacement)


    def addFileControls(self):
        controlPanel = Tkinter.Canvas(self.root)
        controlPanel.pack(side="left")
        #controls
        loadButton=Tkinter.Button(controlPanel, text= "open file", command=self.chooseFile)
        loadButton.pack(side="top")

        saveButton=Tkinter.Button(controlPanel, text= "save changed file", command=self.saveFile)
        saveButton.pack(side="bottom")






    #####EVENT HANDLERS##########



    def chooseFile(self):
        filename = tkFileDialog.askopenfilename()
        if len(filename) > 0:
            self.processFilename(filename)


    def saveFile(self):
        if self.curFileName:
            if os.path.exists(self.RESULT_FOLDER):
                newFileName = self.getResultFileName()
                text = self.getTextFromEntry()
                self.writeTextIntoFile(text, newFileName)
            else:
                tkMessageBox.showinfo("error", "cannot find the results folder:"+self.RESULT_FOLDER)

    def onSpelling(self):
        self.clearPreviousResults(self.SPELLING_ERROR)
        tokens = self.getTokens()
        self.addSpellcheckingResults(self.getTokens())
        for token in tokens:
            replacements = self.getSpellcheckingReplacements(token)
            if replacements:
                self.highlight_pattern(token, self.SPELLING_ERROR)


    def onGoverning(self):
        self.clearPreviousResults(self.AGR_GOV_ERROR)
        self.createTempFile()
        badPairs = self.morphoVerifier.verifyFile(self.TEMP_FILENAME)
        for badPair in badPairs:
            self.highlight_pattern(badPair, self.AGR_GOV_ERROR)




    def on_spelling_error(self, event):
        index = self.getTkinterIndexByClickEvent(event)
        [begOfWord, endOfWord]  = self.getBegOfWordEndOfWordByIndex(index)
        self.curBeg = begOfWord
        self.curEnd = endOfWord
        word = self.getTokenByIndices(begOfWord, endOfWord)
        wordRes = self.getSpellcheckingReplacements(word)
        self.addToSpellcheckingListbox(wordRes)


    def on_edit_spelling_error(self, event):
        self.clearListbox()
        print "edit"
        index = self.getTkinterIndexByClickEvent(event)
        print index
        [begOfWord, endOfWord]  = self.getBegOfWordEndOfWordByIndex(index)
        print begOfWord
        print endOfWord
        #self.textWidget.tag_remove(self.SPELLING_ERROR, begOfWord, endOfWord)

    def on_choose_replacement(self, event):
        if self.curBeg:
            replacement = self.getCurSelectionReplacement()
            self.textWidget.delete(self.curBeg, self.curEnd)
            self.textWidget.insert("insert", replacement)
            self.clearListbox()
            return True




    ###############WORD INDICES################

    def getBegOfWordEndOfWordByIndex(self, index):
        begOfWord = self.textWidget.search(",|\.| ", index, stopindex="1.0", regexp=True,
            backwards=True)
        if begOfWord == "":
            begOfWord = "1.0"
        '''else:
            begOfWord = "%s+1c" %  begOfWord'''
        endOfWord = self.textWidget.search(",|\.| ", index, stopindex=END, regexp=True)
        if endOfWord == "":
            endOfWord = END
        else:
            endOfWord = "%s+1c" %  endOfWord
        return [begOfWord, endOfWord]

    def getTokenByIndices(self, begOfWord, endOfWord):
        return self.textWidget.get(begOfWord, endOfWord).strip('.').strip()

    def getTkinterIndexByClickEvent(self, event):
        return self.textWidget.index("@%s,%s" % (event.x, event.y))


    def highlight_pattern(self, pattern, tag, start="1.0", end="end"):
        #TODO: simplify
        start = self.textWidget.index(start)
        end = self.textWidget.index(end)
        self.textWidget.mark_set("matchStart",start)
        self.textWidget.mark_set("matchEnd",start)
        self.textWidget.mark_set("searchLimit", end)

        count = Tkinter.IntVar()
        pattern = self.escape(pattern)
        while True:
            index = self.textWidget.search("\s?"+pattern+"(\s|\.|^)?", "matchEnd","searchLimit",
                count=count, regexp=True)
            if index == "": break
            self.textWidget.mark_set("matchStart", index)
            self.textWidget.mark_set("matchEnd", "%s+%sc" % (index,count.get()))
            self.textWidget.tag_add(tag, "matchStart","matchEnd")



    ###############SPELLCHECKING CONTROLS###########################
    def addToSpellcheckingListbox(self, wordRes):
        self.clearListbox()
        if wordRes:
            for item in wordRes:
                self.spellcheckingListbox.insert(END, item)

    def clearListbox(self):
        self.spellcheckingListbox.delete(0, END)


    def getCurSelectionReplacement(self):
        curSelectionIndex = self.spellcheckingListbox.curselection()
        if curSelectionIndex:
            return " "+self.spellcheckingListbox.get(int(curSelectionIndex[0]))+" "
        return None


    ###############SPELLCHECKING###########################

    def addSpellcheckingResults(self, tokens):
        self.spellcheckingResult = dict()
        for i in range(0, len(tokens)):
            token = tokens[i]
            if not self.isPunctMark(token) and not isCorrect(token):
                variants = getSuggestions(token, self.NUM_OF_CHANGES, isToUseStats=True)
                self.spellcheckingResult[token]=variants


    def getSpellcheckingReplacements(self, token):
        return self.spellcheckingResult.get(token)

    def isPunctMark(self, token):
        return token in self.PUNCT_MARKS


        ###############UTILITY FUNCTIONS - TEXT WIDGET#####################

    def processFilename(self, filename):
        self.curFileName = filename
        text = self.readFile(filename)
        self.displayText(text)
        self.tokens = self.tokenizer.tokenizeReturnArray(text)

    def getTokens(self):
        return self.tokenizer.tokenizeReturnArray(self.getTextFromEntry())

    def getTextFromEntry(self):
        return self.textWidget.get("1.0", END)

    def displayText(self, text):
        self.textWidget.delete("1.0", END)
        self.textWidget.insert(END, text)

    def clearPreviousResults(self, OLD_TAG):
        self.textWidget.tag_remove(OLD_TAG, "1.0", END)

        ###############UTILITY FUNCTIONS#####################

    def writeTextIntoFile(self, text, filename):
        with codecs.open(filename, 'w', 'utf-8') as fin:
            fin.write(text)

    def getResultFileName(self):
        curFileNameLastPart = self.curFileName.split('/')[-1]
        return self.RESULT_FOLDER+curFileNameLastPart+'_result.txt'



    def readFile(self, filename):
        with codecs.open(filename, 'r', 'utf-8') as fin:
            return fin.read()

    def createTempFile(self):
        text = self.preprocessText(self.getTextFromEntry())
        with codecs.open(self.TEMP_FILENAME, 'w', 'utf-8') as fin:
            fin.write(text)

    def preprocessText(self, text):
        for symbol in self.BAD_SYMBOLS:
            text = text.replace(symbol,'')
        return text

    def escape(self, text):
        return text.replace('.','\.')


TextProcessor()