# coding=utf-8
__author__ = 'gisly'
import codecs, os, re

from src.ru.gisly.CorpusDb import CorpusDb
corpusDb = CorpusDb()

GEO_DELIM = ','

def parseGeoFolder(foldername):
    for filename in os.listdir(foldername):
        parseGeo(os.path.join(foldername, filename))
        print filename
        
        
def parseGeo(filename):
    geoObjects = getFileText(filename).split(GEO_DELIM)
    for geo in geoObjects:
        geo = geo.strip()
        if geo and not isLatin(geo):
            corpusDb.insertGeo({'geo':geo.upper()})  
            
def isLatin(geo):
    return re.match(ur'.*[a-z].*', geo)   
    
def getFileText(filename):
    allLines = ''
    with codecs.open(filename, 'r', 'utf-8') as fin:
        for line in fin:
            allLines+=line
    return allLines

def importStates(stateList):
    with codecs.open(stateList, 'r', 'utf-8') as fin:
        for line in fin:
            corpusDb.insertGeo({'geo':line.strip()})  
                
                
                
#parseGeoFolder("D:\\LingM\\Dictionaries\\names\\geo")
importStates("C://states.txt")
            