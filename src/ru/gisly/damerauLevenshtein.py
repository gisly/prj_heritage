# coding=utf-8
"http://mwh.geek.nz/2009/04/26/python-damerau-levenshtein-distance/"
from src.ru.gisly.DownloadGoogleNGrams import CorpusDB

ADD='A'
DEL='D'
SUB='S'

db=CorpusDB()


def printPath(paths, seq1, seq2, language):
    lastCell=paths[-1][-1]
    while True:
        if 'action' in lastCell:
            operation=lastCell['action']
            operation['lang']=language
            db.insertOperation(operation)
            print str(lastCell['action'])
        if 'prevY' in lastCell:
            lastCellPrevY=lastCell['prevY']
            lastCellPrevX=lastCell['prevX']
            if lastCellPrevX < 0:
                if lastCellPrevY>=0:
                    operation=createAddDict(seq2[lastCellPrevY])
                    operation['lang']=language
                    db.insertOperation(operation)
                    print str(operation)
                break
            elif lastCellPrevY <0:
                if lastCellPrevX>=0:
                    operation=createDelDict(seq1[lastCellPrevX])
                    operation['lang']=language
                    db.insertOperation(operation)
                    print(str(operation))
                break
            lastCell=paths[lastCellPrevX][lastCellPrevY]

        else:
            break

def dameraulevenshtein(seq1, seq2, language=None):
    """Calculate the Damerau-Levenshtein distance between sequences.

    This distance is the number of additions, deletions, substitutions,
    and transpositions needed to transform the first sequence into the
    second. Although generally used with strings, any sequences of
    comparable objects will work.

    Transpositions are exchanges of *consecutive* characters; all other
    operations are self-explanatory.

    This implementation is O(N*M) time and O(M) space, for N and M the
    lengths of the two sequences.

    >>> dameraulevenshtein('ba', 'abc')
    2
    >>> dameraulevenshtein('fee', 'deed')
    2

    It works with arbitrary sequences too:
    >>> dameraulevenshtein('abcd', ['b', 'a', 'c', 'd', 'e'])
    2
    """
    # codesnippet:D0DE4716-B6E6-4161-9219-2903BF8F547F
    # Conceptually, this is based on a len(seq1) + 1 * len(seq2) + 1 matrix.
    # However, only the current and two previous rows are needed at once,
    # so we only store those.
    oneago = None
    thisrow = range(1, len(seq2) + 1) + [0]

    paths=[]

    for i in range(0, len(seq1)):
        newList=[]
        for j in range(0, len(seq2)):
            newList.append(0)
        paths.append(newList)
    for x in xrange(len(seq1)):
        # Python lists wrap around for negative indices, so put the
        # leftmost column at the *end* of the list. This matches with
        # the zero-indexed strings and saves extra calculation.
        twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [x + 1]
        for y in xrange(len(seq2)):
            delcost = oneago[y] + 1
            addcost = thisrow[y - 1] + 1
            subcost = oneago[y - 1] + (seq1[x] != seq2[y])
            thisrow[y] = min(delcost, addcost, subcost)

            cell=dict()
            if delcost <= addcost:
                if delcost <= subcost:
                    cell['prevY']=y
                    cell['prevX']=x-1
                    cell['action']=createDelDict(seq1[x])
                else:
                    cell['prevY']=y-1
                    cell['prevX']=x-1
                    if seq1[x]!= seq2[y]:
                        cell['action']=createSubDict(seq1[x],seq2[y])
            else:
                if addcost <= subcost:
                    cell['prevY']=y-1
                    cell['prevX']=x
                    cell['action']=createAddDict(seq2[y])
                else:
                    cell['prevY']=y-1
                    cell['prevX']=x-1
                    if seq1[x]!= seq2[y]:
                        cell['action']=createSubDict(seq1[x],seq2[y])
            paths[x][y]=cell
            # This block deals with transpositions
            '''if (x > 0 and y > 0 and seq1[x] == seq2[y - 1]
                and seq1[x-1] == seq2[y] and seq1[x] != seq2[y]):
                thisrow[y] = min(thisrow[y], twoago[y - 2] + 1)'''

    printPath(paths, seq1, seq2, language)
    return thisrow[len(seq2) - 1]

def createAddDict(letter):
    return {'operation':ADD, 'letter2':letter}
def createDelDict(letter):
    return {'operation':DEL, 'letter1':letter}
def createSubDict(letter1, letter2):
    return {'operation':SUB, 'letter1':letter1, 'letter2':letter2}


#print dameraulevenshtein(u'еще', u'еше')
