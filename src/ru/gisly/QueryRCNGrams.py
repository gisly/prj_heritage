# coding=utf-8
__author__ = 'gisly'
import codecs
from src.ru.gisly import lemmer

class QueryRCNGrams(object):
    def __init__(self, db):
        self.corpusDB = db

    def processFile(self, filename):
        f=codecs.open(filename,'r', 'utf-8')
        fileText=f.read()
        allWords=lemmer.Tokenizer().tokenizeReturnArray(fileText)
        for i in range(0, len(allWords)-1):
            print self.findRightNeighbours(allWords[i])
            #result=self.getJointScore(allWords[i], allWords[i+1])
            result=self.getJointScore(allWords[i])
            print allWords[i], allWords[i+1], result
        f.close()

    def getJointScore(self, words):
        if len(words)==2:
            return self.corpusDB.getRCScoreBiGram(words[0], words[1])
        else:
            return self.corpusDB.getRCScoreTriGram(words[0], words[1], words[2])

    def findRightNeighbours(self, word1):
        neighbours =  self.corpusDB.getRCRightNeighbours(word1)
        print len(neighbours)
        for n in neighbours:
            print n


#print QueryRCNGrams().getJointScore(u'и',u'не')
