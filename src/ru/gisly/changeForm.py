# coding=utf-8
__author__ = 'gisly'
import sys,os.path

gislyFolder = os.path.dirname(__file__)
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(ruFolder)))
sys.path.append(parentFolder)
import codecs



from pymorphy import get_morph






class Morphology(object):

    morph = get_morph('D:\\LingM\\pymorphy\\ru.sqlite-json\\')
    #morph = get_morph('/home/gisly/pymorphy/ru.sqlite-json')	
    formsToChange = None

    NOUN = u'С'
    VERB = u'Г'
    ADVERB = u'Н'
    ADJECTIVE = u'П'
    BR_ADJECTIVE = u'КР_ПРИЛ'
    PARTICIPLE=u'ПРИЧАСТИЕ'
    PARTICLE = u'ЧАСТ'
    CONJUNCTION = u'СОЮЗ'
    INTERJECTION = u'МЕЖД'
    PRO_ADJECTIVE = u'МС-П'
    PREPOSITION = u'ПРЕДЛ'
    
    
    SINGULAR = u'ед'
    NOMINATIVE = u'им'
    
    THIRD_PERSON = u'3л'
    MASC= u'мр'

    PRESENT=u'нст'
    FUT=u'буд'
    
    
    
    DELIM=','
    DICT_DELIM='_'
    
    FORMS_TO_CHANGE_FILE = 'C://Users/User/Documents/Aptana Studio 3 Workspace/prj_heritage/src/ru/gisly/resources/formsToChange.csv'
    #FORMS_TO_CHANGE_FILE = /home/gisly/prj_heritage/src/ru/gisly/resources'
    def getPos(self, word):
        word = word.upper()
        info = self.getMorphoInfo(word)
        if not info:
            return None
        oldPos = info[0][0]
        for i in range(1, len(info)):
            newPos = info[i][0]
            if newPos!=oldPos:
                return None
        return oldPos

    def getPosesMorphoInfo(self, word):
        word = word.upper()
        info = self.getMorphoInfo(word)
        if not info:
            return None
        poses = set([infoVar[0] for infoVar in info])
        '''for i in range(1, len(info)):
            newPos = info[i][0]
            if newPos!=oldPos:
                return None'''
        return poses, info

    def getLemmaSet(self, word):
        return self.morph.normalize(word.upper())


    def isCertainlyVerbSubject(self, word):
        info = self.getMorphoInfo(word)
        if not info:
            return False
        for info_variant in info:
            if info_variant[0] == self.NOUN and self.SINGULAR in info_variant[1] and\
               self.NOMINATIVE in info_variant[1]:
                return True
        return False


    def isNoun(self, word):
        return self.isPos(word, self.NOUN)

    def isAdverb(self, word):
        return self.isPos(word, self.ADVERB)
    
    def isAdjective(self, word):
        return self.isPos(word, self.ADJECTIVE)  or self.isPos(word, self.BR_ADJECTIVE)


    def isAux(self, word):
        return self.isPosInList(word, [self.PARTICLE, self.INTERJECTION, self.CONJUNCTION, self.PRO_ADJECTIVE])

    def isPos(self, word, POS):
        info = self.getMorphoInfo(word.upper())
        if not info:
            return False
        for info_variant in info:
            if info_variant[0] != POS:
                return False
        return True

    def isPosInList(self, word, posList):
        info = self.getMorphoInfo(word.upper())
        if not info:
            return False
        for info_variant in info:
            if not info_variant[0] in posList:
                return False
        return True

    def isNon3SgVerb(self, word):
        info = self.getMorphoInfo(word)
        if not info:
            return False
        for info_variant in info:
            if info_variant[0] == self.VERB and \
               not (self.THIRD_PERSON in info_variant[1] and self.SINGULAR in info_variant[1]) :
                return True
        return False

    def isNonNomSg(self, info):
        for info_variant in info:
            if self.NOMINATIVE in info_variant[1] and self.SINGULAR in info_variant[1] :
                return False
        return True

    def isFullNonMascNomSgAdj(self, word):
        info = self.getMorphoInfo(word)
        if not info:
            return False
        for info_variant in info:
            if info_variant[0] == self.BR_ADJECTIVE or\
               (info_variant[0] == self.ADJECTIVE and not
               (self.NOMINATIVE in info_variant[1] and self.SINGULAR in info_variant[1] and
                self.MASC in info_variant[1])) :
                return True
        return False


    def getMorphoInfo(self, word):
        info = self.morph.get_graminfo(word)
        if not info:
            return None
        return [[info_variant['class'],info_variant['info'].split(self.DELIM)] for info_variant in info]




    def getFormChangedList(self, ngram0, ngram1):
        poses0_morphInfo0 = self.getPosesMorphoInfo(ngram0)
        if poses0_morphInfo0:
            poses0,morphInfo0=poses0_morphInfo0[0],poses0_morphInfo0[1]
            poses1_morphInfo1 = self.getPosesMorphoInfo(ngram1)
            if poses1_morphInfo1:
                poses1,morphInfo1=poses1_morphInfo1[0],poses1_morphInfo1[1]
                indexToChangePos0Pos1 =  self.getIndexToChange(poses0, poses1)
                if indexToChangePos0Pos1:
                    indexToChange = indexToChangePos0Pos1[0]
                    pos0=indexToChangePos0Pos1[1]
                    pos1=indexToChangePos0Pos1[2]
                    if indexToChange==0:
                        return [self.getNormalized(ngram0, pos0, morphInfo0), ngram1]
                    elif indexToChange==1:
                        if self.isNonNomSg(morphInfo0):
                            return [ngram0, self.getNormalized(ngram1, pos1, morphInfo1)]
        return []

    def getIndexToChange(self, poses0, poses1):
        if self.formsToChange is None:
            self.initFormsToChange()
        for pos0 in poses0:
            for pos1 in poses1:
                key = pos0+self.DICT_DELIM+pos1
                index = self.formsToChange.get(key)
                if index is not None:
                    return index, pos0, pos1
        return None

    def initFormsToChange(self):
        self.formsToChange = dict()
        with codecs.open(self.FORMS_TO_CHANGE_FILE, 'r', 'utf-8') as fin:
            for line in fin:
                lineSplit = line.strip().split(',')
                self.formsToChange[lineSplit[0]]=int(lineSplit[1])

    def getNormalized(self, word, pos, oldInfo):
        if pos==self.NOUN:
            return self.getNominativeNounForm(word)
        if pos==self.VERB:
            return self.get3SgMascVerbForm(word, oldInfo)
        if pos in [self.ADJECTIVE, self.PARTICIPLE]:
            return self.getNominativeSgMascAdjForm(word)
        if pos in [self.PREPOSITION]:
            return word.lower()
        raise Exception('no method for the pos: '+pos)


    def get3SgMascVerbForm(self, word, oldInfo):
        if self.PRESENT in oldInfo[0][1] or self.FUT in oldInfo[0][1]:
            return self.morph.inflect_ru(word.upper(),self.THIRD_PERSON+self.DELIM+self.SINGULAR).lower()
        return self.morph.inflect_ru(word.upper(),self.SINGULAR+self.DELIM+self.MASC).lower()

    def getNominativeNounForm(self, word):
        return self.morph.inflect_ru(word.upper(),self.NOMINATIVE+self.DELIM).lower()

    def getNominativeSgMascAdjForm(self, word):
        return self.morph.inflect_ru(word.upper(),self.NOMINATIVE+self.DELIM+self.MASC).lower()




'''def tryGetAnotherVerbForm(ngram0, ngram1):
    ngram0Upper = ngram0.upper()
    ngram1Upper = ngram1.upper()
    if not isNon3SgVerb(ngram1Upper):
        return None
    if isCertainlyVerbSubject(ngram0Upper):
        return None
    return get3SgVerbForm(ngram1Upper).lower()

def tryGetAnotherNounForm(ngram0, ngram1):
    ngram0Upper = ngram0.upper()
    ngram1Upper = ngram1.upper()
    if not isNoun(ngram0Upper) or not isNonNomSgNoun(ngram1Upper):
        return None
    return getNominativeSgNounForm(ngram0Upper).lower()

def tryGetAnotherAdjForm(ngram0, ngram1):
    ngram0Upper = ngram0.upper()
    ngram1Upper = ngram1.upper()
    if not isAdverb(ngram0Upper) or not isFullNonMascNomSgAdj(ngram1Upper):
        return None
    return getNominativeSgMascAdjForm(ngram1Upper).lower()'''



#print Morphology().get3SgVerbForm(u'приходят')

#listOfForms = Morphology().getFormChangedList(u'минусов',u'такой')
#минусов такой [программы] (минусы такой)
#print ' '.join(listOfForms)

"""listOfForms = Morphology().getFormChangedList(u'очень',u'амбициозная')
#очень амбициозная (очень амбициозный)
print ' '.join(listOfForms)

listOfForms = Morphology().getFormChangedList(u'детства',u'проявляют')
#детства проявляют (детства проявляет)
print ' '.join(listOfForms)


listOfForms = Morphology().getFormChangedList(u'параллельно',u'буду')
#параллельно буду (очень амбициозный)
print ' '.join(listOfForms)


listOfForms = Morphology().getFormChangedList(u'архитектурой',u'России')
#архитектурой России (архитектура России)
print ' '.join(listOfForms)

listOfForms = Morphology().getFormChangedList(u'представляешь',u'своего')
#представляешь своего (представляеm своего)
print ' '.join(listOfForms)

listOfForms = Morphology().getFormChangedList(u'затонули',u'у')
#затонули у (затонул у)
print ' '.join(listOfForms)


#TODO: когда менять время???

listOfForms = Morphology().getFormChangedList(u'это',u'пророчил')
#это пророчил (это пророчит)
print ' '.join(listOfForms)


listOfForms = Morphology().getFormChangedList(u'потратят',u'все')
#потратят все (потратит все)
print ' '.join(listOfForms)

#TODO: средний род->мн. ч.
listOfForms = Morphology().getFormChangedList(u'рубеж',u'уехало')
#рубеж уехало (рубеж уехали)
print ' '.join(listOfForms)

listOfForms = Morphology().getFormChangedList(u'могут',u'роднить')
#могут роднить (может роднить)
print ' '.join(listOfForms)

listOfForms = Morphology().getFormChangedList(u'недавно',u'перебравшимся')
#недавно перебравшимся (недавно перебравшийся)
print ' '.join(listOfForms)

listOfForms = Morphology().getFormChangedList(u'возможностям',u'улучшить')
#возможностям улучшить (возможность улучшить)
print ' '.join(listOfForms)


#TODO: change tense

listOfForms = Morphology().getFormChangedList(u'остаются',u'насовсем')
#остаются насовсем (останется насовсем)
print ' '.join(listOfForms)

#TODO: менять только число!

listOfForms = Morphology().getFormChangedList(u'живется',u'трудовым')
#живется трудовым (живется трудовому)
print ' '.join(listOfForms)






'''


3.6.	 [стали] вводить штраф (находит " ввести штраф", но это ведь не совсем словоизменение, "ввели штраф" нет)

3.8.	индийском купце (индийский купец)
3.9.	это пророчил (это пророчит)
3.10.

3.12.	разная модель ("разные модели" находит)
3.13.
3.14.

3.16.
3.17.	  (живется трудовому)'''

    

'''def isParticle(self, word):
    return isPos(word, PARTICLE)

def isConjunction(self, word):
    return isPos(word, CONJUNCTION) '''









'''newVF =  tryGetAnotherVerbForm(u'своего',u'представляешь')
print newVF '''

#print isAux(u'ИЛИ')


#print tryGetAnotherAdjForm(u'очень',u'амбициозная')
#print newPair[0]
#print newPair[1]"""