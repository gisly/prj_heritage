# coding=utf-8
import codecs
import gzip
import re

import sys,os.path
srcFolder = os.path.dirname(__file__)
gislyFolder = os.path.dirname(os.path.abspath(srcFolder))
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(ruFolder)))
sys.path.append(parentFolder)
from src.ru.gisly.CorpusDb import CorpusDb



from src.ru.gisly.HeritageLogger import HeritageLogger









#extracts an ngram archive into a database
class GoogleNGramExtractor:
    corpusDB=CorpusDb()
    logFileName='D://heritageLog/extractorLog.log'

    step=10000


    GOOGLE_FIELD_SEPARATOR=u'\t'
    GOOGLE_NGRAM_WORD_SEPARATOR=u' '

    GOOGLE_POS_TAG_MARK='_'


    YEAR_OLD = 1950






    #extract a single archive into the database and filter it
    def extractFilterGzipIntoDB(self, gzipFileName):
        with HeritageLogger(self.logFileName) as logger:
            with gzip.open(gzipFileName, 'r') as fin:
                lineCounter=0
                prevNgram0=None
                prevNgram1=None
                isYearAfterYearOld=False
                curTotal=0



                for line in fin:
                    try:
                        line=line.decode('utf-8')
                        csvParts=line.split(self.GOOGLE_FIELD_SEPARATOR)
                        ngramPart=csvParts[0].split(self.GOOGLE_NGRAM_WORD_SEPARATOR)
                        newNgram0=ngramPart[0]
                        newNgram1=ngramPart[1]
                        newYear=int(csvParts[1])


                        if lineCounter==0:

                            newIsAfterOld=newYear>self.YEAR_OLD

                            ngramsFromPreviousFile=self.corpusDB.getFilteredNGram(newNgram0,newNgram1,newIsAfterOld)

                            if ngramsFromPreviousFile.count():
                                ngramFromPreviousFile=ngramsFromPreviousFile[0]
                                curTotal=ngramFromPreviousFile['match_count']
                                self.corpusDB.removeFilteredNGram(ngramFromPreviousFile)



                        if self.GOOGLE_POS_TAG_MARK in newNgram0 or\
                           self.GOOGLE_POS_TAG_MARK in newNgram1:
                            continue



                        if prevNgram0 is not None:
                            if prevNgram0!=newNgram0 \
                                or prevNgram1!=newNgram1 \
                                    or (isYearAfterYearOld==0 and newYear>self.YEAR_OLD):
                                ngram=dict()
                                ngram['ngram0']=prevNgram0
                                ngram['ngram1']=prevNgram1
                                ngram['year']=isYearAfterYearOld
                                ngram['match_count']=curTotal
                                
                                self.corpusDB.insertFilteredNGram(ngram)

  

                                curTotal=0
                        prevNgram0=newNgram0
                        prevNgram1=newNgram1
                        curTotal+=int(csvParts[2])


                        if newYear>self.YEAR_OLD:
                            isYearAfterYearOld=1
                        else:
                            isYearAfterYearOld=0
                        lineCounter+=1
                        if lineCounter % self.step == 0:
                            pass
                            #logger.logToFileConsole('processed:'+str(lineCounter)+' lines')
                    except Exception, e:
                        logger.logToFileConsole('error while parsing ' +gzipFileName+' :' +str(e))



    def addPos(self, gzipFileName, prevLineNgram0,prevLineNgram1):
        with HeritageLogger(self.logFileName) as logger:
            with gzip.open(gzipFileName, 'r') as fin:
                lineCounter=0
                prevNgram0=None
                prevNgram1=None
                newNgram0=None
                newNgram1=None


                isYearAfterYearOld=False
                curTotal=0

                for line in fin:
                    try:

                        line=line.decode('utf-8')
                        csvParts=line.split(self.GOOGLE_FIELD_SEPARATOR)
                        ngramPart=csvParts[0].split(self.GOOGLE_NGRAM_WORD_SEPARATOR)
                        newNgram0=ngramPart[0]
                        newNgram1=ngramPart[1]
                        newYear=int(csvParts[1])

                        if not(self.GOOGLE_POS_TAG_MARK in newNgram0 or self.GOOGLE_POS_TAG_MARK in newNgram1):
                            prevLineNgram0=newNgram0
                            prevLineNgram1=newNgram1
                            continue


                        if self.GOOGLE_POS_TAG_MARK in newNgram0:
                            newNgram0=newNgram0.split(self.GOOGLE_POS_TAG_MARK)[0]
                            if newNgram0=='':
                                continue
                        if self.GOOGLE_POS_TAG_MARK in newNgram1:
                            newNgram1=newNgram1.split(self.GOOGLE_POS_TAG_MARK)[0]
                            if newNgram1=='':
                                continue

                        if (prevLineNgram0 is None or prevLineNgram0 == newNgram0) and \
                           (prevLineNgram1 is None or prevLineNgram1 ==newNgram1):
                            continue

                        prevLineNgram0=newNgram0
                        prevLineNgram1=newNgram1


                        if prevNgram0 is not None:
                            if prevNgram0!=newNgram0\
                               or prevNgram1!=newNgram1\
                            or (isYearAfterYearOld==0 and newYear>self.YEAR_OLD):
                                ngram=dict()
                                ngram['ngram0']=prevNgram0
                                ngram['ngram1']=prevNgram1
                                ngram['year']=isYearAfterYearOld
                                ngram['match_count']=curTotal

                                #Todo
                                self.corpusDB.insertFilteredNGram(ngram)

                                #logger.logToFileConsole('inserted a POS ngram '+prevNgram0+':'+prevNgram1, False, True)

                                curTotal=0
                        prevNgram0=newNgram0
                        prevNgram1=newNgram1
                        curTotal+=int(csvParts[2])


                        if newYear>self.YEAR_OLD:
                            isYearAfterYearOld=1
                        else:
                            isYearAfterYearOld=0
                        lineCounter+=1
                        if lineCounter % self.step == 0:
                            #logger.logToFileConsole('processed:'+str(lineCounter)+' lines')
                            pass
                    except Exception, e:
                        logger.logToFileConsole('error while parsing ' +gzipFileName+' :' +str(e), False, False)

            return [newNgram0,newNgram1]


    def sortFile(self, gzipFileName):
        with gzip.open(gzipFileName, 'r') as fin:
            lineArr=[line for line in fin]
        return sorted(lineArr)



    #extract a single archive into the database and filter it
    def extractFilterGzipTriGramIntoDB(self, gzipFileName, prevNGram):
        with HeritageLogger(self.logFileName) as logger:
            lineArr = self.sortFile(gzipFileName)
            #with gzip.open(gzipFileName, 'r') as fin:
            lineCounter=0
            prevNgram0=prevNGram[0]
            prevNgram1=prevNGram[1]
            prevNgram2=prevNGram[2]
            isYearAfterYearOld=False
            curTotal=0





            for line in lineArr:
                try:
                    line=line.decode('utf-8')
                    csvParts=line.split(self.GOOGLE_FIELD_SEPARATOR)
                    csvPartsNoPos=self.normalizeNGram(csvParts[0])
                    ngramPart=csvPartsNoPos.split(self.GOOGLE_NGRAM_WORD_SEPARATOR)
                    newNgram0=ngramPart[0]
                    newNgram1=ngramPart[1]
                    newNgram2=ngramPart[2]
                    newYear=int(csvParts[1])




                    if newNgram0==self.GOOGLE_POS_TAG_MARK or\
                       newNgram1== self.GOOGLE_POS_TAG_MARK or\
                       newNgram2== self.GOOGLE_POS_TAG_MARK :
                        continue

                    if prevNgram0:
                        if prevNgram0!=newNgram0\
                            or prevNgram1!=newNgram1\
                            or prevNgram2!=newNgram2\
                            or (isYearAfterYearOld==0 and newYear>self.YEAR_OLD):
                                ngram=dict()
                                ngram['ngram0']=prevNgram0
                                ngram['ngram1']=prevNgram1
                                ngram['ngram2']=prevNgram2
                                ngram['year']=isYearAfterYearOld
                                ngram['match_count']=curTotal

                                #self.corpusDB.insertFilteredTriGram(ngram)
                                #print 'I want to insert'+str(ngram)

                                curTotal=0
                    prevNgram0=newNgram0
                    prevNgram1=newNgram1
                    prevNgram2=newNgram2
                    curTotal+=int(csvParts[2])


                    if newYear>self.YEAR_OLD:
                        isYearAfterYearOld=1
                    else:
                        isYearAfterYearOld=0
                    lineCounter+=1
                    if lineCounter % self.step == 0:
                        logger.logToFileConsole('processed:'+str(lineCounter)+' lines')
                except Exception, e:
                    logger.logToFileConsole('error while parsing ' +gzipFileName+' :' +str(e))

        return prevNGram


    #extract a single archive into the database
    def extractGzipIntoDB(self, gzipFileName):
        with HeritageLogger(self.logFileName) as logger:
            with gzip.open(gzipFileName, 'r') as fin:
                lineCounter=0
                for line in fin:
                    try:
                        line=line.decode('utf-8')
                        csvParts=line.split(self.GOOGLE_FIELD_SEPARATOR)
                        ngramPart=csvParts[0].split(self.GOOGLE_NGRAM_WORD_SEPARATOR)
                        ngram=dict()
                        for i in range(0, len(ngramPart)):
                            ngram['ngram'+str(i)]=ngramPart[i]
                        ngram['year']=int(csvParts[1])
                        ngram['match_count']=int(csvParts[2])
                        ngram['filename']=gzipFileName
                        self.corpusDB.insertNGram(ngram)
                        lineCounter+=1
                        if lineCounter % self.step == 0:
                            logger.logToFileConsole('processed:'+str(lineCounter)+' lines')
                    except Exception, e:
                        logger.logToFileConsole('error while parsing '+line + 'in ' +gzipFileName+' :' +str(e), True, False)

    def normalizeNGram(self, nGram):
        nGram=re.sub(r'_([A-Z]|\.)+',u'',nGram)
        nGram=re.sub(r'\d*(0|[5-9]) ',u'5 ',nGram)

        nGram=re.sub(r'\d*(0|[2-9])([2-4]) ',u'2 ',nGram)

        nGram=re.sub(r'(0|[2-9])?1 ',u'1 ',nGram)

        nGram=re.sub(r'\d+([1-4]) ',u'5 ',nGram)

        nGram=re.sub(r'([2-4]) ',u'2 ',nGram)

        nGram=re.sub(r'\d*(0|[2-9])1 ',u'1 ',nGram)

        return nGram




    #log the fact that a file has been extracted successfully
    def logSuccessInDB(self, gzipFileName):
        event=dict()
        event['filename']=gzipFileName
        event['result']=1
        self.corpusDB.insertEvent(event)

    #log the fact that a file has been extracted with an error
    def logFailureInDB(self, gzipFileName):
        event=dict()
        event['filename']=gzipFileName
        event['result']=0
        self.corpusDB.insertEvent(event)




class RuscorporaNGramExtractor:
    corpusDB=CorpusDb()
    logFileName='H:\\heritageLog\\ruscorporaExtractorLog.log'

    step=10000


    RC_FIELD_SEPARATOR='\t'

    PUNCT_MARKS=[',','.','-','/','(',')','--', u'»', u'«', '.:', ',:', ':', '<', '>']

    #extract a single archive into the database
    def extractBiGzipIntoDB(self, gzipFileName):
        with HeritageLogger(self.logFileName) as logger:
            with codecs.open(gzipFileName, 'r', 'utf-8') as fin:
                lineCounter=0
                for line in fin:
                    try:
                        csvParts=line.split(self.RC_FIELD_SEPARATOR)
                        ngram=dict()
                        ngram['match_count']=int(csvParts[0])
                        ngram['ngram0']=csvParts[1].strip()

                        part2 = csvParts[2].strip()
                        if part2=='':
                            ngram['ngram1']=csvParts[3].strip()
                            self.corpusDB.insertRCBiGram(ngram)
                        else:
                            ngram['ngram1']=part2
                            ngram['ngram2']=csvParts[3].strip()
                            self.corpusDB.insertRCTriGram(ngram)
                        lineCounter+=1
                        if lineCounter % self.step == 0:
                            logger.logToFileConsole('processed:'+str(lineCounter)+' lines')
                    except Exception, e:
                        logger.logToFileConsole('error while parsing '+line + 'in ' +gzipFileName+' :' +str(e))

    def extractTriGzipIntoDB(self, gzipFileName):
        with HeritageLogger(self.logFileName) as logger:
            with codecs.open(gzipFileName, 'r', 'utf-8') as fin:
                lineCounter=0
                for line in fin:

                    try:
                        #csvParts=line.split(self.RC_FIELD_SEPARATOR)
                        csvParts=re.split(ur'\t+', line)
                        ngram=dict()
                        ngram['match_count']=int(csvParts[0])
                        isTrueTrigram = True
                        for i in range(1, len(csvParts)):
                            curPart = csvParts[i].strip()
                            if curPart in self.PUNCT_MARKS:
                                isTrueTrigram = False
                                break
                            elif curPart!='':
                                ngram['ngram'+str(i-1)]=curPart

                        if isTrueTrigram:
                            self.corpusDB.insertRCTriGram(ngram)
                        lineCounter+=1
                        if lineCounter % self.step == 0:
                            logger.logToFileConsole('processed:'+str(lineCounter)+' lines')
                    except Exception, e:
                        logger.logToFileConsole('error while parsing '+line + 'in ' +gzipFileName+' :' +str(e))






#d=GoogleNGramsDownloader()
#d.downloadAllArchives()
"""dirName = "D://Heritage/ngramsToAdd/to"
prev1 = None
prev2 = None
for filename in os.listdir(dirName):
    prev1, prev2 = GoogleNGramExtractor().addPos(dirName+'/'+filename, prev1, prev2)
    #pass"""
    
    
dirName = "D://Heritage/ngramsToAdd/mn"
for filename in os.listdir(dirName):
    GoogleNGramExtractor().extractFilterGzipIntoDB(dirName+'/'+filename)
    
    
dirName = "D://Heritage/ngramsToAdd/mn"
prev1 = None
prev2 = None
for filename in os.listdir(dirName):
    prev1, prev2 = GoogleNGramExtractor().addPos(dirName+'/'+filename, prev1, prev2)
    

#GoogleNGramsDownloader().downloadAllArchivesForLetter('a','H:\\heritageData')
'''letterStr='abcdefghijklmnopqrstuvwxyz'
print letterStr
downloader=GoogleNGramsDownloader()
for letter in letterStr:
    downloader.downloadAllArchivesForLetter(letter,'H:\\heritageData\\'+letter+'NGrams')'''
'''[prevNgram0,prevNgram1]=GoogleNGramExtractor().addPos('H:\\heritageData\\tNgrams\\split\\googlebooks-rus-all-2gram-20120701-ta2.gz', None, None
)
print prevNgram0
print prevNgram1


'''

'''ngram=GoogleNGramExtractor().corpusDB.getFilteredNGramAnyYear(u'бабабаб', u'ssss')
print ngram.count()'''

#RuscorporaNGramExtractor().extractBiGzipIntoDB("E:\\LingM\\Heritage\\RCNGrams\\2grams-3\\2grams-3.txt")
#RuscorporaNGramExtractor().extractTriGzipIntoDB("E:\\LingM\\Heritage\\RCNGrams\\3grams-3\\3grams-3.txt")
#GoogleNGramExtractor().extractFilterGzipTriGramIntoDB("I:\\heritageData3Grams\\b\\split\\googlebooks-rus-all-3gram-20120701-b_0.gz", ['','',''])
