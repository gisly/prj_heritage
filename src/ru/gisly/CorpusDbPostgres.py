# coding=utf-8
__author__ = 'gisly'
import psycopg2
import datetime

class CorpusDbPostgres(object):
    def __init__(self,host='localhost',port=27017,dbName='googleNGRAMS'):
        self.connection=psycopg2.connect("dbname="+dbName+" user=postgres password=password")



    '''def __del__(self):
        print 'dying'
        self.connection.disconnect() '''

    '''def insertNGram(self, ngram):
        try:
            colNGrams=self.db.nGrams
            colNGrams.insert(ngram)
        except Exception, e:
            print(str(e))


    def insertFilteredNGram(self, ngram):
        try:
            colNGrams=self.db.filteredGoogleNGrams
            colNGrams.insert(ngram)
            #colNGrams.update(ngram, {'upsert':True})
        except Exception, e:
            print(str(e))

    def insertFilteredTriGram(self, ngram):
        try:
            colNGrams=self.db.filteredGoogleTriGrams
            colNGrams.insert(ngram)
            #colNGrams.update(ngram, {'upsert':True})
        except Exception, e:
            print(str(e))'''


    def getFilteredNGram(self, ngram0, ngram1, year):
        cur = self.connection.cursor()
        cur.execute("SELECT match_count FROM ngrams.googlebigrams where ngram0=%s AND ngram1=%s AND year=%s", 
                    [ngram0, ngram1, year])
        return cur.fetchone()
        #return self.db.filteredGoogleNGrams.find({'ngram0':ngram0,'ngram1':ngram1, 'year':year})
    def getFilteredNGramAnyYear(self, ngram0, ngram1):
        return self.db.filteredGoogleNGrams.find({'ngram0':ngram0,'ngram1':ngram1})

    '''def removeFilteredNGram(self, ngram):
        self.db.filteredGoogleNGrams.remove(ngram)'''

    def insertRCBiGram(self, ngram):
        try:
            colRCNBigrams=self.db.rcBigrams
            colRCNBigrams.insert(ngram)
        except Exception, e:
            print(str(e))
            
    
    def getScoreNGram(self, word1, word2):
        cur = self.connection.cursor()
        cur.execute("SELECT match_count FROM ngrams.googlebigrams where ngram0=%s AND ngram1=%s AND year=%s", 
                    [word1, word2, False])
        res1 = cur.fetchone() 
        if res1 is None:
            res1=[0]
        cur.execute("SELECT match_count FROM ngrams.googlebigrams where ngram0=%s AND ngram1=%s AND year=%s", 
                    [word1, word2, True])
        res2 = cur.fetchone() 
        if res2 is None:
            res2=[0]
        return res1[0]+res2[0]
    
    def release(self):
        pass

    '''def insertRCTriGram(self, ngram):
        try:
            colRCNTrigrams=self.db.rcTrigrams
            colRCNTrigrams.insert(ngram)
        except Exception, e:
            print(str(e))

    def insertWikibigram(self, word0, word1):
        try:
            colWikibigrams=self.db.wikiBigrams
            dataFound=colWikibigrams.find({'ngram0':word0, 'ngram1':word1})
            flag=False
            for item in dataFound:
                flag=True
                colWikibigrams.remove(item)
                colWikibigrams.insert({'ngram0':word0, 'ngram1':word1,'count':item['count']+1})
                break
            if not flag:
                colWikibigrams.insert({'ngram0':word0, 'ngram1':word1,'count':1})
        except Exception, e:
            print(str(e))


    def insertEvent(self, event):
        colEvents=self.db.events
        colEvents.insert(event)


    def insertPrepositionCase(self, preposition, case):
        colPreps=self.db.prepositions
        prepData=colPreps.find({'prep':preposition, 'case':case})
        flag=False
        for item in prepData:
            flag=True
            colPreps.remove(item)
            colPreps.insert({'prep':preposition, 'case':case,'count':item['count']+1})
        if not flag:
            colPreps.insert({'prep':preposition, 'case':case, 'count':1})


    def insertOperation(self, operation):
        colOperations=self.db.operations
        opData=colOperations.find(operation)
        flag=False
        for item in opData:
            flag=True
            operation['count']=item['count']+1
            colOperations.remove(item)
            colOperations.insert(operation)
        if not flag:
            operation['count']=1
            colOperations.insert(operation)


    def groupRCCorpus(self):
        colRCNGrams=self.db.rcNGrams
        colRCNGramsSorted=self.db.rcNGrams.find({'grouped':None}).sort({'ngram0':1,'ngram1':1})
        prevWord0=None
        prevWord1=None
        totalCount=0
        for ngram in colRCNGramsSorted:
            if prevWord0 is not None:
                if prevWord0!=ngram['word0'] and  prevWord1!=ngram['word1'] :
                    newNgram=dict()
                    newNgram['word0']=prevWord0
                    newNgram['word1']=prevWord1
                    newNgram['match_count']=totalCount
                    newNgram['grouped']='yes'
                    colRCNGrams.insert(newNgram)
                else:
                    totalCount+=ngram['match_count']
            prevWord0 =ngram['word0']
            prevWord1 =ngram['word1']


    #get a list of words which occur on the right of 'word' in al bigrams
    def getRightNeighbours(self, word):
        colNGrams=self.db.filteredGoogleNGrams
        rightNeighbours=[]
        colNGramsFound=colNGrams.find({'ngram0':word})
        for colNGram in colNGramsFound:
            word2Index='ngram1'
            if word2Index in colNGram:
                word2 = colNGram[word2Index]
                if not word2 in rightNeighbours:
                    rightNeighbours.append(word2)
        return rightNeighbours



        #get a list of words which occur on the right of 'word' in al bigrams
    def getRCRightNeighbours(self, word):
        colNGrams=self.db.filteredGoogleNGrams
        rightNeighbours=[]
        colNGramsFound=colNGrams.find({'ngram0':word})
        for colNGram in colNGramsFound:
            word2Index='ngram1'
            if word2Index in colNGram:
                word2 = colNGram[word2Index]
                if not word2 in rightNeighbours:
                    rightNeighbours.append(word2)
        return rightNeighbours

    # sum the match_counts of all the bigrams where word1 is on the left of word2
    def getScoreNGram(self, word1, word2):
        colNGrams=self.db.filteredGoogleNGrams
        #TODO: remove
        ngrams0=colNGrams.find({'ngram0':word1, 'ngram1':word2, 'year':0})
        ngrams1=colNGrams.find({'ngram0':word1, 'ngram1':word2, 'year':1})
        res0 = 0
        for res in ngrams0:
            res0 = res['match_count']
            break

        res1 = 0
        for res in ngrams1:
            res1 = res['match_count']
            break

        return res0 + res1
        #TODO: remove
        ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2}).limit(2)
        #return self.getMatchCount(ngrams)
        return sum([matchCount for matchCount in (ngram['match_count'] for ngram in ngrams)])


    # sum the match_counts of all the bigrams where word1 is on the left of word2
    def getRCScoreBiGram(self, word1, word2):
        colNGrams=self.db.rcBigrams
        ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2})
        #ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2}).limit(1)
        return self.getMatchCount(ngrams)
        #return sum([matchCount for matchCount in (ngram['match_count'] for ngram in ngrams)])

    def getRCScoreTriGram(self, word1, word2, word3):
        colNGrams=self.db.rcTrigrams
        ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2, 'ngram2':word3})
        #ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2, 'ngram2':word3}).limit(2)
        return self.getMatchCount(ngrams)
        #return sum([matchCount for matchCount in (ngram['match_count'] for ngram in ngrams)])

    def getWikiScoreBiGram(self, word1, word2):
        colNGrams=self.db.wikiBigrams
        ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2})
        return self.getMatchCount(ngrams, 'count')
    
    def getRCScoreMorphBigram(self, word1, word2):
        colNGrams=self.db.morphNgrams
        ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2})
        #ngrams=colNGrams.find({'ngram0':word1, 'ngram1':word2}).limit(1)
        return self.getMatchCount(ngrams)

    def getMatchCount(self, ngrams, countWord = 'match_count'):
        result = 0
        for ngram in ngrams:
            result = ngram[countWord]
            break
        return result

    def release(self):
        print 'releasing connection'
        self.connection.disconnect()'''
       
       
db = CorpusDbPostgres()

print str(datetime.datetime.now())

print db.getScoreNGram(u'ты',u'иду')

print str(datetime.datetime.now())
       

