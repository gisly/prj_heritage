# coding=utf-8
__author__ = 'gisly'

import urllib2
import shutil
import urlparse
import os
from src.ru.gisly.HeritageLogger import HeritageLogger


class GoogleNGramsDownloader:
    GOOGLE_RUSSIAN_LINK_BEG='http://storage.googleapis.com/books/ngrams/books/googlebooks-rus-all-'
    GOOGLE_RUSSIAN_LINK_END='gram-20120701-'
    GOOGLE_ARCHIVE_EXT='.gz'
    ARCHIVE_FOLDER='H:\\heritageData'

    logFileName='H:\\heritageLog\\googleDownloader'
    ALL_LETTERS='abcdefghijklmnopqrstuvwxyz0123456789_'

    #download all gzip files into the db
    def downloadAllArchives(self, archiveFolder=ARCHIVE_FOLDER, n=2, allLetters=ALL_LETTERS, isOnlyOne=False):
        allLetterLength=len(allLetters)
        if isOnlyOne:
            for i in range(0, allLetterLength):
                fileName=self.GOOGLE_RUSSIAN_LINK_BEG+str(n)+self.GOOGLE_RUSSIAN_LINK_END+allLetters[i]
                self.download(fileName+self.GOOGLE_ARCHIVE_EXT,archiveFolder+'\\'+allLetters[i])
        else:

            for i in range(0, allLetterLength):
                for j in range(0, allLetterLength):
                    fileName=self.GOOGLE_RUSSIAN_LINK_BEG+str(n)+self.GOOGLE_RUSSIAN_LINK_END+allLetters[i]+allLetters[j]
                    self.download(fileName+self.GOOGLE_ARCHIVE_EXT,archiveFolder+'\\'+allLetters[i])



                #download all gzip files into the db
    '''def downloadAllArchivesForLetter(self, letter, archiveFolder=ARCHIVE_FOLDER):

        #allTheLetters = list(string.lowercase)+['_']
        allTheLetters='_'
        allLetterLength=len(allTheLetters)


        for i in range(0, allLetterLength):
            fileName=self.GOOGLE_RUSSIAN_BIGRAM_LINK+letter+allTheLetters[i]
            self.download(fileName+self.GOOGLE_ARCHIVE_EXT,archiveFolder)'''

    #download a gzip file
    def download(self,url, folderName, fileName=None):
        with HeritageLogger(self.logFileName) as logger:
            r=None
            try:
                r = urllib2.urlopen(urllib2.Request(url))
                fileName = folderName+'\\'+(fileName or self.getFileName(url,r))
                logger.logToFileConsole('started downloading '+fileName)
                if os.path.exists(fileName):
                    logger.logToFileConsole('file already exists '+url+':'+fileName )
                    #self.extractorLog.debug('error while downloading '+url + ':file already exists'+fileName)
                    return
                if not os.path.exists(folderName):
                    os.makedirs(folderName)
                with open(fileName, 'wb') as f:
                    shutil.copyfileobj(r,f)
                    #creating a marker file to show that the archive is ready to be extracted
                fMarker=open(fileName.replace('gz','txt'),'w')
                fMarker.close()
            except Exception, e:
                logger.logToFileConsole(str(e))
                #self.extractorLog.debug('error while downloading '+url + ':' + str(e))
            finally:
                if r is not None:
                    r.close()

    #get the file name given the url
    def getFileName(self,url,openUrl):
        if 'Content-Disposition' in openUrl.info():
            # If the response has Content-Disposition, try to get filename from it
            cd = dict(map(
                lambda x: x.strip().split('=') if '=' in x else (x.strip(),''),
                openUrl.info()['Content-Disposition'].split(';')))
            if 'filename' in cd:
                filename = cd['filename'].strip("\"'")
                if filename: return filename
                # if no filename was found above, parse it out of the final URL.
        return os.path.basename(urlparse.urlsplit(openUrl.url)[2])