# coding=utf-8
__author__ = 'gisly'
import codecs

DELIM = ','


def writeCities(filename):
    cityArr = parseCities(filename)
    with codecs.open(filename+'_cities.txt','w', 'utf-8') as fout:
        fout.write(DELIM.join(cityArr))


def parseCities(filename):
    cityArr=[]
    with codecs.open(filename, 'r', 'utf-8') as fin:
        for line in fin:
            lineAfterCities = line.split('`city_` VALUES (')[-1]
            cityArr.append(lineAfterCities.split(DELIM)[4].replace('\'', '').split('(')[0].strip())
            
    return set(cityArr)

def writeCountries(filename):
    arr = parseCountries(filename)
    with codecs.open(filename+'_countries.txt','w', 'utf-8') as fout:
        fout.write(DELIM.join(arr))


def parseCountries(filename):
    arr=[]
    with codecs.open(filename, 'r', 'utf-8') as fin:
        for line in fin:
            lineAfter = line.split('`country_` VALUES (')[-1]
            arr.append(lineAfter.split(DELIM)[2].replace('\'', '').split('(')[0].strip())
            
    return set(arr)

def writeRegions(filename):
    arr = parseRegions(filename)
    with codecs.open(filename+'_regions.txt','w', 'utf-8') as fout:
        fout.write(DELIM.join(arr))


def parseRegions(filename):
    arr=[]
    with codecs.open(filename, 'r', 'utf-8') as fin:
        for line in fin:
            lineAfter = line.split('`region_` VALUES (')[-1]
            arr.append(lineAfter.split(DELIM)[3].replace('\'', '').split('(')[0].strip())
            
    return set(arr)

writeCities('D://LingM/Dictionaries/names/geoCities.txt')
writeCountries('D://LingM/Dictionaries/names/geoCountries.txt')
writeRegions('D://LingM/Dictionaries/names/geoRegions.txt')



