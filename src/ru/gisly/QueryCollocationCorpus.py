# coding=utf-8
__author__ = 'gisly'
import codecs
import urllib2
import lxml.html
import lemmer

class QueryCollocationCorpus:
    collocationUrl='http://corpus.leeds.ac.uk/cgi-bin/cqp.pl?q='
    corpusSection='&c=I-RU'
    contextSize='&contextsize=60'
    sort='&sort1=word&sort2=right'
    terminate='&terminate=100'
    searchType='&searchtype=colloc'
    methodType='&mistat=on'
    contextType='cleft=0&cright=1&cfilter=&da=word'





    def processFile(self, filename):
        f=codecs.open(filename,'r', 'utf-8')
        fileText=f.read()
        allWords=lemmer.Tokenizer().tokenizeReturnArray(fileText)
        for i in range(0, len(allWords)-1):
            result=self.getJointScore(allWords[i], allWords[i+1])
        f.close()

    def getJointScore(self, word1, word2):
        collSet=self.getCollocationSet(word1)
        return self.getJointScoreInCollocationSet(word1,word2, collSet)


    def getCollocationSet(self, word):
        collocationUrl=self.createCollocationURL(word)
        data=self.getHTMLData(collocationUrl)
        return self.getCollocationDataList(data)

    def createCollocationURL(self, word):
        collocationUrl=''.join([self.collocationUrl,word.encode('utf-8'),
                                                             self.corpusSection,self.contextSize,
                                                             self.sort,self.terminate,
                                                             self.searchType,self.methodType,
                                                             self.contextSize, self.sort,
                                                             self.terminate,self.searchType,
                                                             self.methodType, self.contextType])
        return collocationUrl


    def getHTMLData(self,url):
        usock = urllib2.urlopen(url)
        data = usock.read()
        usock.close()
        return data

    def getCollocationDataList(self, htmlData):
        html   = lxml.html.fromstring(htmlData)
        allCollocationRows = html.xpath('//tbody/tr')
        collocationSet=[]
        for collocationRow in allCollocationRows:
            collocationInfo=dict()
            collocationData = collocationRow.xpath('.//td')
            colText = collocationData[0].text_content().split(' ')
            collocationInfo['word0'] =  colText[0]
            collocationInfo['word1'] =  colText[1]
            collocationInfo['joint'] = collocationData[1].text_content()
            collocationSet.append(collocationInfo)
        return collocationSet


    def getJointScoreInCollocationSet(self,word0,word1,collSet):
        for collocation in collSet:
            if word0 == collocation['word0'] and word1 == collocation['word1']:
                return collocation['joint']
        return 0


queryCollocationCorpus=QueryCollocationCorpus()
#print queryCollocationCorpus.getJointScore(u'любимый',u'правда')

'''queryCollocationCorpus.processFile('C:\\Users\\user\\IdeaProjects\\HeritageProject\\PythonModule\\resources\\test.txt'''''