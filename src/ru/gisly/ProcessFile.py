# coding=utf-8
__author__ = 'gisly'
import re
import ConfigParser

#from py4j.java_gateway import JavaGateway


import sys,os.path
srcFolder = os.path.dirname(__file__)
gislyFolder = os.path.dirname(os.path.abspath(srcFolder))
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(ruFolder)))
sys.path.append(parentFolder)
import codecs
from src.ru.gisly.QueryRCNGrams import QueryRCNGrams
from src.ru.gisly.QueryWikiNGrams import QueryWikiNGrams
from src.ru.gisly.QueryGoogleCorpus import QueryGoogleCorpus
from src.ru.gisly.lemmer import Lemmer, Tokenizer
from src.ru.gisly.CorpusDb import CorpusDb
#from src.ru.gisly.CorpusDbPostgres import CorpusDbPostgres
from src.ru.gisly.changeForm import Morphology
from src.ru.gisly.makeSemanticSubstitutions import getSemanticSubstitutions


#calling collocation processors
class CollocationProcessor(object):
    #processor connector objects
    google=None
    sharov=None
    disco=None
    rc=None

    wiki = None

    corpusDb = None
    morphology = Morphology()
    
    replacementDict = {u'ё':u'е',u'Ё':u'е', '<':'', '>':'', u'«':'', u'»':'', u'“':'',u'”':'', '/':u' или '}
    
    config = ConfigParser.RawConfigParser()
    CONFIG_NAME = 'heritageSettings.ini'
    HEURISTIC_CONFIG_NAME = 'heuristicsSettings.ini'
    
    
    def __init__(self):
        self.config.read(os.path.join(os.path.dirname(__file__), self.CONFIG_NAME))
        self.config.read(os.path.join(os.path.dirname(__file__), self.HEURISTIC_CONFIG_NAME))


    def processListOfWords(self, listOfWords, processorList,resultFileName, ignorePunct = False):
        self.initialiseProcessConnectors(processorList)
        return self.processListOfWordsWithProcessors(listOfWords,processorList,resultFileName, ignorePunct)

    def processListOfWordsWithProcessors(self, listOfWords,processorList,resultFileName,ignorePunct = False):
        with codecs.open(resultFileName, 'w', 'utf-8') as resultFile:
            self.processBigramsWithProcessors(processorList, listOfWords, resultFile, ignorePunct)
        self.releaseResources()


    def processBigramsWithProcessors(self, processorList, listsOfWordsPerSentence, resultFile, ignorePunct = False):
        prevWord = None
        resultFile.write('word0\tword1\t'+'\t'.join(processorList)+'\t' + '\t'.join(['normalized' + processorName for processorName in processorList])+'\r\n')
        for wordsBySentence in listsOfWordsPerSentence:
            prevWord = None
            for currentWordNotStripped in wordsBySentence:
                currentWord = currentWordNotStripped.strip()
                if prevWord:
                    if ignorePunct or self.noPunctuationBorder(prevWord, currentWord):
                        resultFile.write(prevWord+'\t'+currentWord)
                        for processorName in processorList:
                            score, normalizedScore = self.getBigramScore(prevWord, currentWord, processorName)
                            resultFile.write('\t'+str(score)+'\t'+"%.2f" % normalizedScore)
                        resultFile.write('\r\n')      
                        
                if not (ignorePunct and not currentWord[0].isalpha()):
                    prevWord = currentWord
            
    #TODO: change!
    def processBigramsWithProcessorsInstantResult(self, processorList, listOfWords, ignorePunct = False):
        self.initialiseProcessConnectors(processorList)
        prevWord = None
        result=[]
        for currentWordNotStripped in listOfWords:
            currentWord = currentWordNotStripped.strip()
            if currentWord!='' and prevWord and (not ignorePunct or (currentWord[0].isalpha() and prevWord[0].isalpha())): 
                for processorName in processorList:
                    newScore=self.processBigramWithProcessor(prevWord,currentWord,processorName)
                    if newScore==0 and \
                            (self.hasCapitalLetters(prevWord) or self.hasCapitalLetters(currentWord)):
                        newScore=self.processBigramWithProcessor(prevWord.lower(),currentWord.lower(),processorName)
                    if newScore==0:
                        possibleFormList = self.morphology.getFormChangedList(prevWord.lower(), currentWord.lower())
                        if possibleFormList:
                            newScore=self.processBigramWithProcessor(possibleFormList[0],possibleFormList[1],processorName)
                    #TODO: add complex reasoning
                    if newScore <= 20:
                        result.append([prevWord + ' ' +currentWord])      
            prevWord=currentWord
        return result

    #TODO: сделать хорошую нормализацию
    def getBigramScore(self, word1, word2, processor):
        self.initialiseProcessConnectors([processor])
        
        REMOVE_CAPITALS = self.config.getboolean('BIGRAMS', 'remove_capitals')
        MORPHOLOGY = self.config.getboolean('BIGRAMS', 'morphology')
        SEMANTIC = self.config.getboolean('BIGRAMS', 'semantic')
        WIKI = self.config.getboolean('BIGRAMS', 'wiki')
        
        if word1=='EMPTY' or word2=='EMPTY' or word1=='' or word2=='':
            return 10000,10000
        
        if word1[0].isalpha() and word2[0].isalpha():
            score=self.processBigramWithProcessor(self.normalizeWord(word1),self.normalizeWord(word2),processor)
 
            
            if score==0 and REMOVE_CAPITALS and (self.hasCapitalLetters(word1) or self.hasCapitalLetters(word2)):
                score=self.processBigramWithProcessor(word1.lower(),word2.lower(),processor)
                    
            if score==0 and MORPHOLOGY:
                possibleFormList = self. morphology.getFormChangedList(word1.lower(), word2.lower())
                if possibleFormList:
                    score=self.processBigramWithProcessor(possibleFormList[0],possibleFormList[1],processor)
                    
            if score==0 and SEMANTIC:
                semanticSubstitutions1 = getSemanticSubstitutions(word1)
                semanticSubstitutions2 = getSemanticSubstitutions(word2)
                            
                if len(semanticSubstitutions1)>1 or len(semanticSubstitutions2)>1:
                    for semSubst1 in semanticSubstitutions1:
                        for semSubst2 in semanticSubstitutions2:
                        #print semSubst1+':'+semSubst2
                            score = max(score,  self.processBigramWithProcessor(semSubst1,semSubst2,processor))
                            """if score==0:
                                #TODO: change?
                                score = self.corpusDb.getWikiScoreBiGram(word1, word2)
                                
                                if score!=0:
                                    print 'wiki:'+word1+':'+word2+':'+str(score)
                                    score = score*1000"""
            limit  = self.calculateBigramLimitWithProcessor(word1, word2, processor)
            if limit == 0:
                normalizedScore = 0
            else:
                normalizedScore = 10000 * score/float(limit)
        else:
            score = 100000
            normalizedScore = score
            limit = 0
        return score, normalizedScore   
  
                         

    def processTrigramsWithProcessors(self, processorList, listOfWords, resultFile):
        prevPrevWord = None
        prevWord = None
        for currentWordNotStripped in listOfWords:
            currentWord = currentWordNotStripped.strip()
            if prevWord and prevPrevWord:
                resultFile.write('\t'.join([prevPrevWord, prevWord, currentWord]))
                for processorName in processorList:
                    if processorName=='rc':
                        newScore=self.processTrigramWithProcessor(prevPrevWord,
                            prevWord,currentWord,processorName)
                        resultFile.write('\t'+str(newScore))
                resultFile.write('\r\n')
            prevPrevWord = prevWord
            prevWord = currentWord


    def processBigramWithProcessor(self,prevWord,currentWord,processorName):
        try:
            if processorName=='sharov':
                return int(self.sharov.getJointScore(prevWord, currentWord))
            elif processorName=='disco':
                return self.disco.getJointScore(prevWord,currentWord)
            elif processorName=='google':
                prevWord = self.normalizeForGoogle(prevWord)
                currentWord = self.normalizeForGoogle(currentWord)
                return self.google.getJointScore(prevWord, currentWord)
            elif processorName=='rc':
                return self.rc.getJointScore([prevWord, currentWord])
            raise Exception("no such processor:"+processorName)
        except Exception,e:
            raise Exception(u' '.join(["error when calling ",processorName," for", prevWord,currentWord,str(e)]))
        
    def calculateBigramLimitWithProcessor(self,prevWord,currentWord,processorName):
        try:
            if processorName=='sharov':
                raise Exception("limit calculation not implemented:"+processorName)
            elif processorName=='disco':
                raise Exception("limit calculation not implemented:"+processorName)
            elif processorName=='google':
                prevWord = self.normalizeForGoogle(prevWord)
                currentWord = self.normalizeForGoogle(currentWord)
                return self.google.getLimit(prevWord, currentWord)
            elif processorName=='rc':
                raise Exception("limit calculation not implemented:"+processorName)
            raise Exception("no such processor:"+processorName)
        except Exception,e:
            raise Exception(u' '.join(["error when calculating the limit (",processorName,") for", prevWord,currentWord,str(e)]))

    def processTrigramWithProcessor(self,prevPrevWord,prevWord,currentWord,processorName):
        try:
            if processorName=='sharov':
                raise Exception('unimplemented')
            elif processorName=='disco':
                raise Exception('unimplemented')
            elif processorName=='google':
                raise Exception('unimplemented')
            elif processorName=='rc':
                return self.rc.getJointScore([prevPrevWord, prevWord, currentWord])
            return 0
        except Exception,e:
            raise Exception(u' '.join(["error when calling ",processorName," for",
                                     prevPrevWord, prevWord,currentWord,str(e)]))
            
            
    
            
            
            
    ###########UTILS#########



    def initialiseProcessConnectors(self, processorList):
        if 'google' in processorList and self.google is None:
            if self.corpusDb is None:
                host = self.config.get('DATABASES', 'host')
                port = self.config.getint('DATABASES', 'port')
                databaseName = self.config.get('DATABASES', 'dbname')
                self.corpusDb = CorpusDb(host, port, databaseName)
            self.google=QueryGoogleCorpus(self.corpusDb)
        """if 'sharov' in processorList and self.sharov is None:
            self.sharov=QueryCollocationCorpus()
        if 'gateway' in processorList and self.disco is None:
            # connect to the JVM
            gateway=JavaGateway()
            self.disco=gateway.entry_point"""
        if 'rc' in processorList and self.rc is None:
            if self.corpusDb is None:
                self.corpusDb = CorpusDb()
            self.rc=QueryRCNGrams(self.corpusDb)

        if 'wiki' in processorList and self.wiki is None:
            if self.corpusDb is None:
                self.corpusDb = CorpusDb()
            self.wiki=QueryWikiNGrams(self.corpusDb)

    def releaseResources(self):
        if self.corpusDb is not None:
            self.corpusDb.release()


    def hasCapitalLetters(self, word):
        return any(letter.isupper for letter in word)
    
    #google uses -- instead of -
    def normalizeForGoogle(self, word):
        return word.replace(u'–', u'--')
    
    def normalizeWord(self, word):
        for key, value in self.replacementDict.items():
            word=word.replace(key,value)
        return word
    
    def noPunctuationBorder(self, prevWord, currentWord):
        return currentWord[0].isalpha() and prevWord[0].isalpha()






#the main class for processing a file
class HeritageFileProcessor(object):
    config = ConfigParser.RawConfigParser()
    CONFIG_NAME = 'heritageSettings.ini'


    punctList=[',','.','--','-','!','\'\'','``', u'«', u'»']
    replacementDict = {u'ё':u'е',u'Ё':u'е', '<':'', '>':'', u'«':'', u'»':'', u'“':'',u'”':'', '/':u' или '}

    tokenizer=Tokenizer()
    processorCaller=CollocationProcessor()


    """def printDirectory(self, dirName):
        for filename in os.listdir(dirName):
            try:
                self.printText(dirName+"\\"+filename)
            except Exception, e:
                print str(e)

    def printText(self, fileName):
        listOfWords=self.getListOfWords(fileName)
        with codecs.open(fileName+'_.txt', 'w', 'utf-8') as fout:
            for i in range(0, len(listOfWords)-1):
                fout.write(listOfWords[i]+'\t'+listOfWords[i+1]+'\r\n')"""
                
    def __init__(self):
        self.config.read(os.path.join(os.path.dirname(__file__), self.CONFIG_NAME))


    def callProcessorsDirectory(self, dirName, processorList, ignorePunct=False):
        """
        applies the processors from <processorList> to each file in <dirname>
        """
        for filename in os.listdir(dirName):
            try:
                self.callProcessorsFile(os.path.join(dirName, filename), processorList, ignorePunct)
            except Exception, e:
                print str(e)

    def callProcessorsFile(self, fileName, processorList, ignorePunct=False):
        """
        applies the processors from <processorList> to <filename>
        """
        resultFileName=self.getResultFileName(fileName,processorList)
        listOfWords=self.getListOfWordsFromFile(fileName)
        print("starting processing "+fileName)
        self.processorCaller.processListOfWords(listOfWords,processorList,resultFileName, ignorePunct)
        print("processed "+fileName+" and wrote results into "+resultFileName)
        return resultFileName
            
            
    def callProcessorsText(self, processorList, text, ignorePunct=False):
        """
        applies the processors from <processorList> to <filename>
        """
        listOfWords=self.getListOfWords(text)
        return self.processorCaller.processBigramsWithProcessorsInstantResult(processorList,listOfWords,ignorePunct)

    def getListOfWordsFromFile(self, fileName):
        text = self.readTextFromFile(fileName)
        return self.getListOfWords(text)
    
    def getListOfWords(self, text):
        text=self.preprocessText(text)
        #produces strange results!
        return self.tokenizer.tokenizeReturnArray(text)

        """words = re.split(ur'[\.+|,|;|:|—|!|\?|"|\(|\)|\-|\s]+', text)
        return [word for word in words if (not word in self.punctList) and word]"""

    def preprocessText(self, text):
        #the first character may be ASCII 239
        if text.startswith(u'\ufeff'):
            text = text[1:]
        text=text.strip()
        for key, value in self.replacementDict.items():
            text=text.replace(key,value)
        return text

    def readTextFromFile(self, fileName):
        with codecs.open(fileName,'r','utf-8') as fin:
            return fin.read()

    def getResultFileName(self, fileName, processorList):
        origFileName=fileName.split('\\')[-1].split('.')[0]
        resultFolder = self.config.get('FOLDERNAMES', 'processingresultfolder')
        return resultFolder+'\\'+origFileName+'_'.join(processorList)+'.txt'


def main():
    heritageProcessor=HeritageFileProcessor()
    if len(sys.argv)<3:
        print('usage1: python ProcessFile.py punct/ignore filename google rc')
        print('usage2: python ProcessFile.py punct/ignore dir      foldername google rc')
        return
    ignorePunct = not(sys.argv[1]=='punct')
    print ignorePunct
    if sys.argv[2]=='dir':
        heritageProcessor.callProcessorsDirectory(sys.argv[3],sys.argv[4:], ignorePunct)
    else:
        heritageProcessor.callProcessorsFile(sys.argv[2],sys.argv[3:], ignorePunct)


if __name__ == "__main__":
    main()
    
    