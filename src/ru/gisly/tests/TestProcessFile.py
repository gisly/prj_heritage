#encoding=utf-8
__author__ = 'gisly'
import codecs
import unittest

import sys,os.path
testsFolder = os.path.dirname(__file__)
srcFolder = os.path.dirname(os.path.abspath(testsFolder))
gislyFolder = os.path.dirname(os.path.abspath(srcFolder))
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(ruFolder)))
sys.path.append(parentFolder)

from src.ru.gisly.ProcessFile import HeritageFileProcessor

FILENAME = './tests/spellingCorrected_krivinaSpletnya.txt'
class TestSequenceProcessFilePunctGoogle(unittest.TestCase):
    
    def testFile(self):
        curFilename = os.path.join(os.path.dirname(__file__), FILENAME)
        expectedResultsFilename = os.path.join(os.path.dirname(__file__), 
                                                    FILENAME.replace('.txt','')+'google_expected.txt')
    
        heritageProcessor=HeritageFileProcessor()
        receivedResultsFilename = heritageProcessor.callProcessorsFile(curFilename, ['google'], False)
        
        expectedLines = self.readLinesFromFile(expectedResultsFilename)
        receivedLines = self.readLinesFromFile(receivedResultsFilename)
        assert(expectedLines==receivedLines)
                    
                    
    def readLinesFromFile(self, filename):
        lines = []
        with codecs.open(filename, 'r', 'utf-8') as fin: 
            lines = [line for line in fin]
        return lines
                
                
                
    
        