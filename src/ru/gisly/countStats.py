# coding=utf-8
__author__ = 'gisly'
import codecs
import os
import xml.etree.ElementTree as ET
import damerauLevenshtein



NOTE_ATTRIB='note'
SPELLING_NOTE='orpho'
SPELLING_FON_NOTE='orpho/fon'
SPELLING_DOUBLE_NOTE='orpho, orpho'
EXT='.xhtml'

PUNCT_MARKS = [',','.', '...','--','-','!','\'\'','``', u'«', u'»', '(', ')']

def countSpellingStatisticsFile(filename, lang):
    try:
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print filename
        root=ET.parse(filename)
        allFragments = root.findall('.//fragment')
        for i in range(0, len(allFragments)-1):

            fragment2=allFragments[i+1]
            anaChildren=fragment2.findall('.//ana')
            if anaChildren and NOTE_ATTRIB in anaChildren[0].attrib and \
               anaChildren[0].attrib[NOTE_ATTRIB] in [SPELLING_NOTE,SPELLING_FON_NOTE, SPELLING_DOUBLE_NOTE] and i%2==0:
                    fragment1=allFragments[i]
                    origText=''
                    if fragment1.text:
                        origText= stripPunctMarks(fragment1.text)
                    anaChildrenOrig=fragment1.findall('.//ana')
                    if anaChildrenOrig:
                        origText+=' '.join([stripPunctMarks(anaChild.tail) for anaChild in anaChildrenOrig if anaChild.tail is not None])
                    correctedText=' '.join([anaChild.tail for anaChild in anaChildren if anaChild.tail is not None])

                    print origText+"=>"+correctedText
                    damerauLevenshtein.dameraulevenshtein(correctedText, origText, lang)
                    print "=========================================="



    except Exception, e:
        print("error:"+str(e)+" in "+filename)


def countSpellingStatisticsFolder(folder, lang):
    for filename in os.listdir(folder):
        if EXT in filename:
            countSpellingStatisticsFile(folder+'\\'+filename, lang)

def replaceWW_rawFile(file):
    with codecs.open(file, 'r', 'utf-8') as fin:
        text=fin.read()
    with codecs.open(file, 'w', 'utf-8') as fout:
        fout.write(text.replace('<w_raw', '<w_raw>').replace('<w>','<w_raw>').replace('</w>', '</w_raw>'))

def replaceWW_raw(folder):
    for filename in os.listdir(folder):
        if EXT in filename:
            replaceWW_rawFile(folder+'\\'+filename)



def stripPunctMarks(text):
    if text[-1] in PUNCT_MARKS:
        text=stripPunctMarks(text[:-1])
    if text[0] in PUNCT_MARKS:
        text=stripPunctMarks(text[1:])
    return text



folderName='E:\\LingM\\Heritage\\20130212_Heritage\\heritage_2\\from_Olesya'
#folderName=u'E:\\LingM\\Heritage\\heritage_done\\проверенная разметка(до asp)\\Егор-январь2012\\Егор-январь2012\\Данила\\'
#folderName=u'E:\\LingM\\Heritage\\heritage_done\\Егор-апрель2012'
lang='EN'
#replaceWW_raw(folderName)
countSpellingStatisticsFolder(folderName, lang)