# coding=utf-8
__author__ = 'gisly'
class TokenVariants:
    token = None
    variants = []
    def isHavingVariants(self):
        return len(self.variants)>0
    def getVariantByNum(self, variantNum):
        return self.variants[variantNum]
    def getFirstNVariants(self, limit):
        return self.variants[limit:]
    def getToken(self):
        return self.token
