# coding=utf-8
from src.ru.gisly.spellchecking.SpellingOperation import SpellingOperation

__author__ = 'gisly'
import codecs


class StatisticSpellchecker(object):
    STAT_FILE_PATH = 'E:\\LingM\\Heritage\\operations\\operationsSorted.csv'
    STAT_FILE_DELIM = '\t'
    LIMIT = 10
    statistics = []
    letterTransformations = dict()
    REPLACEMENT_NUM = 1
    def __init__(self):
        self.cacheStatisticsFromFile()
    def cacheStatisticsFromFile(self):
        self.statistics = dict()
        with codecs.open(self.STAT_FILE_PATH, 'r', 'utf-8') as fin:
            #the header
            fin.readline()
            for line in fin:
                lineParts = line.split(self.STAT_FILE_DELIM)
                #spellingOperation = SpellingOperation(lineParts[1],lineParts[2],lineParts[3])
                #self.statistics.append([count, spellingOperation])

                letterBecame = lineParts[3].strip()
                opList = self.letterTransformations.get(letterBecame)
                newOperation = SpellingOperation([1],lineParts[2],letterBecame, int(lineParts[0]))
                if opList:
                    opList.append(newOperation)
                else:
                    self.letterTransformations[letterBecame] = [newOperation]


    def getStatisticalSuggestions(self, word, numOfReplacements):
        #for i in range(0, len(word)):
        #return self.getVariants(word, 0, '')
        mostFrequentSpellingOps = self.getNMostFrequentSpellingOperations(word, numOfReplacements)
        return self.applySpellingOps(word, mostFrequentSpellingOps)


    def getNMostFrequentSpellingOperations(self, word, n):
        '''maxFreq = 0
        maxFreqSpellingOp = None
        for letter in word:
            opList = self.letterTransformations.get(letter)
            if opList:
                freqOpList = opList[0]
                if freqOpList[0]>maxFreq:
                    maxFreq = opList[0]
                    maxFreqSpellingOp = SpellingOperation(freqOpList[1],freqOpList[2], letter)
        return maxFreqSpellingOp '''
        opList = [self.letterTransformations.get(letter)[0] for letter in word if self.letterTransformations.get(letter) ]
        opList = sorted(opList, key=lambda x:x.count, reverse=True)
        return opList[0:n]


    def applySpellingOps(self, word, spellingOps):
        return [self.replaceRegardingSpellingOp(word, spellingOp) for spellingOp in spellingOps]
        '''for spellingOp in spellingOps:
            word = self.replaceRegardingSpellingOp(word, spellingOp)
        return word'''

    def replaceRegardingSpellingOp(self, word, spellingOp):
        return word.replace(spellingOp.letterBecame, spellingOp.letterWas, self.REPLACEMENT_NUM)


#print StatisticSpellchecker().getStatisticalSuggestions(u'мэр', 1)



