# coding=utf-8
import enchant
class PyEnchantSpellchecker(object):
    d = enchant.Dict("ru_RU")
    def isCorrect(self, word):
        return self.d.check(word)

    def getSuggestions(self, word):
        return self.d.suggest(word)

