# coding=utf-8
__author__ = 'gisly'
import NorvigSpellchecker

class SpellcheckingServer(object):


    norvigSpellchecker = NorvigSpellchecker.NorvigSpellchecker()


    def checkTokenArray(self, tokenArray):
        """
        processes a list of tokens
        returns the indices of tokens which are misspellt and a list of replacements for corresponding indices
        """

        wrongSpelltWordsIndices = []
        replacements=[]



        for i in range(0, len(tokenArray)):
            token = tokenArray[i]
            spellingCheckResult = self.norvigSpellchecker.correct(token)
            if spellingCheckResult != token:
                wrongSpelltWordsIndices.append(i)
                replacements.append(spellingCheckResult)
        return wrongSpelltWordsIndices, replacements
            
            
"""result = checkTokenArray([u'мама', u'жыр'])
print result[0]
res1 = result[1]
for element in res1:
    print element"""
    
