# coding=utf-8
__author__ = 'Peter Norvig'
#http://norvig.com/spell-correct.html
import re, collections, codecs, os, pickle


class NorvigSpellchecker(object):
    alphabet = u'абвгдежзийклмнопрстуфхцчшщъыьэюя'
    TRAINING_FOLDER='./trainingFolder'
    MODEL_STORAGE='C://Users/User/Documents/Aptana Studio 3 Workspace/prj_heritage/src/ru/gisly/spellchecking/model'
    
    
    def __init__(self):
        self.NWORDS=self.unpickleTrainedModel()
    
    
    
    def words(self, text): return re.findall('\w+', text.lower(), re.UNICODE) 
    
    def train(self, features):
        model = collections.defaultdict(lambda: 1)
        for f in features:
            model[f] += 1
        return model
    
    
    def getWordsFromFile(self, filename):
        wordlist=[]
        with codecs.open(filename, 'r', 'utf-8') as fin:
            for line in fin:
                wordlist+=self.words(line)
        return wordlist    
    
    def getWordsFromDir(self, dirName):
        wordlist=[]
        for filename in os.listdir(dirName):
            fullName = os.path.join(dirName, filename)
            if os.path.isdir(fullName):
                wordlist+=self.getWordsFromDir(fullName)
            else:
                wordlist+=self.getWordsFromFile(fullName)
        return wordlist
            
    def edits1(self, word):
        splits     = [(word[:i], word[i:]) for i in range(len(word) + 1)]
        deletes    = [a + b[1:] for a, b in splits if b]
        transposes = [a + b[1] + b[0] + b[2:] for a, b in splits if len(b)>1]
        replaces   = [a + c + b[1:] for a, b in splits for c in self.alphabet if b]
        inserts    = [a + c + b     for a, b in splits for c in self.alphabet]
        return set(deletes + transposes + replaces + inserts)
    
    def known_edits2(self,word):
        return set(e2 for e1 in self.edits1(word) for e2 in self.edits1(e1) if e2 in self.NWORDS)
    
    def known(self, words): return set(w for w in words if w in self.NWORDS)
    
    def correct(self, word):
        candidates = self.known([word]) or self.known(self.edits1(word)) or self.known_edits2(word) or [word]
        return max(candidates, key=self.NWORDS.get)
    
    def pickleTrainedModel(self):
        NWORDS = dict(self.train(self.getWordsFromDir(self.TRAINING_FOLDER)))
        pickle.dump(NWORDS, open(self.MODEL_STORAGE,'wb'))
        print('pickled successfully to '+self.MODEL_STORAGE)
    
    def unpickleTrainedModel(self):
        model = collections.defaultdict(lambda: 1)
        model.update(pickle.load(open(self.MODEL_STORAGE,'rb')))
        print('loaded successfully from '+self.MODEL_STORAGE)
        return model
    



#NorvigSpellchecker().pickleTrainedModel()





#print NorvigSpellchecker().correct(u'мама')
    
