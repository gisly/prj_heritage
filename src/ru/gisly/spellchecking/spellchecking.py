# coding=utf-8
__author__ = 'gisly'
from src.ru.gisly.spellchecking.StatisticSpellchecker import StatisticSpellchecker
from src.ru.gisly.spellchecking.PyEnchantSpellchecker import PyEnchantSpellchecker
pyenchantSpellchecker = PyEnchantSpellchecker()
statisticSpellchecker = StatisticSpellchecker()

badGoodSymbols={u'ё':u'о', u'«':'"', u'»':'"'}

def isCorrect(word):
    normalizedWord = normalize(word)
    return pyenchantSpellchecker.isCorrect(normalizedWord)

def getSuggestions(word, numOfChanges, isToUseStats):
    if isToUseStats:
        result = processStatisticalSuggestions(statisticSpellchecker.getStatisticalSuggestions(word,
                                                                        numOfChanges))
    else:
        result=[]
    pyEnchantSuggestions = pyenchantSpellchecker.getSuggestions(word)
    result+=pyEnchantSuggestions
    return set(result)

def processStatisticalSuggestions(statSuggestions):
    res=[]
    for statSuggestion in statSuggestions:
        if isCorrect(statSuggestion):
            res+=[statSuggestion]
        else:
            res+= pyenchantSpellchecker.getSuggestions(statSuggestion)
    return res

def normalize(word):
    for badSymbol, goodSymbol in badGoodSymbols.items():
        word= word.replace(badSymbol, goodSymbol)
    return word


'''pyEnchantRes = pyenchantSpellchecker.getSuggestions(u'скучаюсь')
myRes = getSuggestions(u'скучаюсь', 2, True)
for res in  pyEnchantRes:
    print res
print '==============='
for myResStr in  myRes:
    print myResStr'''


