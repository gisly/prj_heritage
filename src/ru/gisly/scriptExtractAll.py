# coding=utf-8
__author__ = 'gisly'
import codecs
import gzip
import sys,os.path
import time

gislyFolder=os.path.dirname(__file__)
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(ruFolder)))
sys.path.append(parentFolder)
from src.ru.gisly.DownloadGoogleNGrams import GoogleNGramExtractor
from src.ru.gisly.HeritageLogger import HeritageLogger

class ScriptExtractor(object):
    cycle_times=300
    extractor=GoogleNGramExtractor()

    logFileName='H:\\heritageLog\extractingLog.log'
    folderName='I:\\heritageData3Grams\\'

    #if we see a marker text file in the directory, we try to extract
    #the corresponding gzip

    def mainCycle(self, letter):
        with HeritageLogger(self.logFileName) as logger:
            curFolderName=self.folderName+letter+'Ngrams\\split'
            for i in range(0,self.cycle_times):
                for filename in os.listdir(curFolderName):
                    if '.txt' in filename:
                        gzipName=filename.replace('txt','gz')
                        logger.logToFileConsole('started extracting '+filename)
                        try:
                            self.extractor.extractFilterGzipIntoDB(curFolderName+'\\'+gzipName)
                            logger.logToFileConsole('extracted '+gzipName)
                            self.extractor.logSuccessInDB(gzipName)
                        except Exception,e:
                            logger.logToFileConsole('error when extracting '+gzipName+':'+str(e))
                            self.extractor.logFailureInDB(gzipName)
                        finally:
                            os.remove(curFolderName+'\\'+filename)
                            logger.logToFileConsole('removed file '+filename)


    def mainCyclePOS(self, letter):
        with HeritageLogger(self.logFileName) as logger:
            curFolderName=self.folderName+letter+'Ngrams\\split'
            prevNgram0=None
            prevNgram1=None
            for i in range(0,self.cycle_times):
                for filename in os.listdir(curFolderName):
                    if '.txt' in filename:
                        gzipName=filename.replace('txt','gz')
                        logger.logToFileConsole('started extracting '+filename)
                        try:
                            [prevNgram0,prevNgram1]=self.extractor.addPos(curFolderName+'\\'+gzipName, prevNgram0, prevNgram1)
                            logger.logToFileConsole('extracted '+gzipName)
                            self.extractor.logSuccessInDB(gzipName)
                            time.sleep(1)
                        except Exception,e:
                            logger.logToFileConsole('error when extracting '+gzipName+':'+str(e))
                            self.extractor.logFailureInDB(gzipName)
                        finally:
                            os.remove(curFolderName+'\\'+filename)
                            logger.logToFileConsole('removed file '+filename)




    def mainCycleTri(self, letter):
        prev=['','','']
        with HeritageLogger(self.logFileName) as logger:
            curFolderName=self.folderName+letter+'\\split'
            for i in range(0,self.cycle_times):
                for filename in os.listdir(curFolderName):
                    if '.txt' in filename:
                        gzipName=filename.replace('txt','gz')
                        logger.logToFileConsole('started extracting '+filename)
                        try:
                            prev = self.extractor.extractFilterGzipTriGramIntoDB(curFolderName+'\\'+gzipName, prev)
                            print prev
                            logger.logToFileConsole('extracted '+gzipName)
                            self.extractor.logSuccessInDB(gzipName)
                        except Exception,e:
                            logger.logToFileConsole('error when extracting '+gzipName+':'+str(e))
                            self.extractor.logFailureInDB(gzipName)
                        finally:
                            os.remove(curFolderName+'\\'+filename)
                            logger.logToFileConsole('removed file '+filename)

    def addTextFiles(self, letter):
        curFolderName=self.folderName+letter+'Ngrams\\split'
        for filename in os.listdir(curFolderName):
            if '.gz' in filename:
                textFile=codecs.open(curFolderName+'\\'+filename.replace('.gz','.txt'),'w')
                textFile.close()

    def runAll(self):
        letters='u'
        for letter in letters:
            ScriptExtractor().mainCycle(letter)

    def runAllPOS(self):
        letters='u'
        for letter in letters:
            ScriptExtractor().mainCyclePOS(letter)


    def runAllTri(self):
        letters='i'
        for letter in letters:
            ScriptExtractor().mainCycleTri(letter)



'''letters='u'
for letter in letters:
    ScriptExtractor().addTextFiles(letter)'''

#ScriptExtractor().addTextFiles('e')
#ScriptExtractor().runAll()
#ScriptExtractor().runAllPOS()
ScriptExtractor().runAllTri()
'''ScriptExtractor().extractor.extractFilterGzipTriGramIntoDB(
    'I:\\heritageData3Grams\\g\\split\\googlebooks-rus-all-3gram-20120701-gr75.gz', ['','',''])'''

'''with gzip.open('I:\\heritageData3Grams\\aaa\\split\\googlebooks-rus-all-3gram-20120701-in121.gz', 'r') as fin:
    lineArr=[line for line in fin]
with codecs.open('C:\\aaa.txt', 'w', 'utf-8') as fout:
    for line in lineArr:
        fout.write(line.decode('utf-8')) '''





