# coding=utf-8
__author__ = 'gisly'
import gzip
import os
import sys,os.path
gislyFolder=os.path.dirname(__file__)
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(ruFolder)))
sys.path.append(parentFolder)
from src.ru.gisly.HeritageLogger import HeritageLogger


class ArchiveSplitter(object):
    logFileName='H:\\heritageLog\splitterLog.log'


    MB_TO_BYTE_RATIO=1000000
    NUM_OF_LINES_PER_MB=280000

    cycle_times=100
    tempFolder='split'

    #
    def splitFile(self, filename, sizeLimitMB=1,createMarkerTextFiles=True):
        """
        splits a gzip archive filename into files, each of which is sizeLimit Mb
        creates marker text files if necessary
        """
        with HeritageLogger(self.logFileName) as logger:
            limit=self.getMaxNumberOfLinesInFile(sizeLimitMB)
            fileNum=0
            newFileName=self.createNewFileName(filename,fileNum,'gz')
            self.handleMarkerTextFile(createMarkerTextFiles,newFileName)
            fout=gzip.open(newFileName,'w')
            with gzip.open(filename, 'r') as fin:
                lineCounter=0
                linePrevBlock=0
                logger.logToFileConsole('started processing:'+filename)
                for line in fin:
                    #line=line.decode('utf-8')
                    try:
                        lineCounter+=1

                        if lineCounter-linePrevBlock>=limit:
                            fileNum+=1
                            linePrevBlock=lineCounter

                            fout.close()
                            newFileName=self.createNewFileName(filename,fileNum,'gz')
                            fout=self.getNextFile(newFileName)

                            self.handleMarkerTextFile(createMarkerTextFiles,newFileName)
                        fout.write(line)
                    except Exception,e:
                        logger.logToFileConsole('error when processing'+filename+':'+str(e), False, True)
            fout.close()
            self.handleMarkerTextFile(createMarkerTextFiles,newFileName)
            logger.logToFileConsole('successully processed:'+filename)



    def getNextFile(self, newFileName):
        return gzip.open(newFileName,'w')

    def getMaxNumberOfLinesInFile(self,sizeLimitMB):
        return sizeLimitMB*self.NUM_OF_LINES_PER_MB

    def handleMarkerTextFile(self, createMarkerTextFiles,newFileName):
        if createMarkerTextFiles:
            textFileName=newFileName.replace('gz','txt')
            fText=open(textFileName,'w')
            fText.close()

    def createMarkerTextFile(self, filename):
        textFileName=filename.replace('gz','txt')
        fText=open(textFileName,'w')
        fText.close()

    def createNewFileName(self, fileName, fileNum, ext):
        filenameArr=fileName.split('\\')
        newFilenameLast=filenameArr[-1].replace('.gz',str(fileNum)+'.'+ext)

        newFileName='\\'.join(filenameArr[:-1])+'\\'+self.tempFolder+'\\'+newFilenameLast

        return newFileName


    def mainCycle(self,folderName,sizeLimit):
        with HeritageLogger(self.logFileName) as logger:
            for i in range(0,self.cycle_times):
                for filename in os.listdir(folderName):
                    if '.txt' in filename:
                        gzipName=filename.replace('txt','gz')
                        logger.logToFileConsole('started extracting '+filename)
                        try:
                            self.splitFile(folderName+'\\'+gzipName,sizeLimit)
                            logger.logToFileConsole('split '+gzipName)
                            os.remove(folderName+'\\'+filename)
                        except Exception,e:
                            logger.logToFileConsole('error when splitting '+gzipName+':'+str(e))
                        finally:
                            logger.logToFileConsole('removed file '+filename)



#ArchiveSplitter().mainCycle('H:\\heritageData\\aNgrams',5)
#ArchiveSplitter().splitFile('I:\\heritageData3Grams\\o\\googlebooks-rus-all-3gram-20120701-os.gz',2)
'''for letter in 'ce':
    ArchiveSplitter().mainCycle('I:\\heritageData3Grams\\'+letter,5)'''



