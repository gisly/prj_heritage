# coding=utf-8


__author__ = 'gisly'
import sys
import re
import codecs
import os
import time





srcFolder = os.path.dirname(__file__)
evalRes = os.path.dirname(os.path.abspath(srcFolder))
gislyFolder = os.path.dirname(os.path.abspath(evalRes))
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(ruFolder)))
sys.path.append(parentFolder)

from src.ru.gisly.changeForm import Morphology

from src.ru.gisly.ProcessFile import CollocationProcessor


class TextError(object):
    def __init__(self, errorCode, errorDescription):
        self.errorCode = errorCode
        self.errorDescription = errorDescription
    
    def __str__(self, *args, **kwargs):
        return self.errorCode+':'+self.errorDescription
        

class ResultEvaluator(object):
    NOT_ERROR = '-1'
    PUNCT_ERROR = '-2'
    SPACE_SPELLING = '-3'
    CAP_SPELLING = '-4'
    EXTRA_PUNCTUATION = 'EP'
    WRONG_PUNCTUATION = 'WP'
    
    
    SPELLING_ERROR_EX = 'S'
    SPELLING_ERROR_NON_EX = 'BS'
    
    WORD_BUILDING = 'CW'
    GOVERNING_VP = 'GVP'
    GOVERNING_NOUN = 'GN'
    WRONG_PARONYM = 'PP'
    
    WRONG_LIGHT_VERB = 'Light'
    
    
    CONSTR_ERROR = 'Constr'
    AGREEMENT_ERROR = 'A' 
    WRONG_PREP = 'PR'
    EXTRA_WORD = 'EW' 
    WRONG_FORM = 'F'
    
    CCAL  = 'CCal'
    
    COMP_ERROR = 'Comp'

    
    
    NOT_ERROR_TYPES=[NOT_ERROR]

    IGNORE = [PUNCT_ERROR, SPACE_SPELLING, CAP_SPELLING, EXTRA_PUNCTUATION, WRONG_PUNCTUATION]
   
    ERRORS = [SPELLING_ERROR_EX,
                    SPELLING_ERROR_NON_EX,
                    
                    WORD_BUILDING,
                    GOVERNING_VP,
                    GOVERNING_NOUN ,
                    WRONG_PARONYM ,
                    
                    WRONG_LIGHT_VERB ,
                    
                    
                    CONSTR_ERROR ,
                    AGREEMENT_ERROR , 
                    WRONG_PREP ,
                    EXTRA_WORD ,
                    WRONG_FORM,
                    
                    CCAL ,
                    
                    COMP_ERROR ]
   
    
    #the default limit for a processor
    PROC_SCORE_LIM = {'rc':0, 'google':1, 'googleNormalized':100}
    #the limit for a preposition
    PREP_SCORE_LIM = {'rc':10, 'google':5000, 'googleNormalized':100}
    #the limit for a modal word
    MODAL_SCORE_LIM = {'rc':10, 'google':9000, 'googleNormalized':100}
    #the limit for an auxiliary
    AUX_SCORE_LIM = {'rc':10, 'google':1000, 'googleNormalized':100}

    DELIM = '\t'
    ERROR_DELIM = ','
    WORD_DELIM = ' '

    NGRAM_ORDER = 2


    procList=[]
    prepList = []
    modalWordList = []

    #arrays for counting stats

    processorTrueError = dict()
    processorFalseError = dict()
    processorTrueCorr = dict()
    processorFalseCorr = dict()

    processorsInitialized = False


    PREP_LIST_FILE = evalRes+"/resources/prepositionList.txt"
    MODAL_LIST_FILE = evalRes+"/resources/modalVerbs.txt"
    
    morph = Morphology()
    
    collocationProcessor = None
    
    def printLimitResults(self, foldername, minLimit, maxLimit):
        """
        prints out f-scores for different values of the default limit
        """
        for i in range(minLimit, maxLimit):
            self.PROC_SCORE_LIM = {'rc':0, 'google':i}


            self.processorTrueError = dict()
            self.processorFalseError = dict()
            self.processorTrueCorr = dict()
            self.processorFalseCorr = dict()
            self.procList=[]
            self.processorsInitialized = False




            self.evaluateFolder(foldername)
            
            print str(i)+'\t'+str(self.countPrecision('google')) +'\t'+str(self.countRecall('google'))


    def evaluateFolder(self, foldername, procList=['rc', 'google']):
        """
        evaluates each file in a folder
        """
        print("starting to analyze "+foldername)
        self.initEvaluation(procList)
        self.analyzeFilesInFolder(foldername)       
        self.printResults(foldername)


    def analyzeFilesInFolder(self, foldername):
        for filename in os.listdir(foldername):
            fullFilename = os.path.join(foldername, filename)
            if os.path.isfile(fullFilename):
                self.evaluateFile(fullFilename)
                self.numOfFiles+=1

    def evaluateFile(self, filename):
        """
        compares the results of machine processing and manual processing
        """

        self.curFile = filename
        with codecs.open(filename, 'r', 'utf-8') as fin:
            self.initProcessors(fin.readline())
            for line in fin:

                try:
                    self.evaluateLine(line)
                except Exception, e:
                    print line +'_'+filename+':'+str(e)
                    
                self.numOfLines+=1


    



    def evaluateLine(self, line):
        """
        compares the manual score and the processors' score
        """
        
        lineParts = line.strip().split('\t')
        errorTypes =  lineParts[-1].split(self.ERROR_DELIM)
        procScores = [round(float(lineParts[i])) for i in range(self.NGRAM_ORDER, len(lineParts)-1)]
        for errorType in errorTypes:
            #because of troubles with punctuation
            #we now skip lines which contain punctuation marks or punctuation errors
            if not self.isIgnored(errorType) and not self.containsBadSymbols([lineParts[0],lineParts[1]]):
                if self.isError(errorType):
                    self.evaluateError(procScores, errorType, lineParts[0]+' '+lineParts[1])
                else:
                    self.evaluateNotError(procScores, errorType, lineParts[0]+' '+lineParts[1])

    def printResults(self, folderName):
        """prints out the statistics for the error processing
        """
        resultFoldername = os.path.join(folderName, 'result')
        if not os.path.exists(resultFoldername):
            os.makedirs(resultFoldername)
        
        resultFilename = os.path.join(resultFoldername, 'result.csv')
        
        with codecs.open(resultFilename, 'w', 'utf-8') as fout:
            fout.write('foldername\t'+folderName+'\r\n')
            fout.write('numberOfFiles\t'+str(self.numOfFiles)+'\r\n')
            fout.write('totalBigrams\t'+str(self.numOfLines)+'\r\n')
            
            totalNumOfErrors = self.getTotalNumOfErrors("ALL")
            fout.write('totalErrors\t'+str(totalNumOfErrors)+'\r\n')
            errorRate = 100*totalNumOfErrors/float(self.numOfLines)
            fout.write('errorRate\t'+str(errorRate)+' %\r\n')
            
            fout.write('numOfProcessors\t'+str(len(self.procList))+'\r\n')
            
            procCount = 0
            
            for processor in self.procList:
                
                fout.write("============================================\r\n")
                
                fout.write('processorName#'+str(procCount)+'\t'+processor+'\r\n')
                
                fout.write('errorType\tALL\r\n')
 
                prec =   self.countPrecision(processor, "ALL")
                recall =  self.countRecall(processor, "ALL")
                f_score = 2*prec*recall/(prec+recall)
                fout.write('precision\t'+str(prec)+'\r\n')
                fout.write('recall\t'+str(recall)+'\r\n')
                fout.write('f-score\t'+str(f_score)+'\r\n')

                
                
                for errorType in self.ERRORS:
                    fout.write('errorType\t'+errorType+'\tnumOfOccurrences\t'+str(self.getTotalNumOfErrors(errorType))+'\r\n')
                    fout.write('errorType\t'+errorType+'\trecall\t'+str(self.countRecall(processor, errorType))+'\r\n')
                    
                if processor=='google':
                    fout.write("======================FALSE ERRORS=====================\r\n")
                    for errType, errors in self.processorFalseError[processor].iteritems():
                        for element in errors:
                            fout.write(element+'\r\n')
                            
                            
                    fout.write("======================FALSE CORR=====================\r\n")
                    for errType, errors in self.processorFalseCorr[processor].iteritems():
                        for element in errors:
                            fout.write(element+'\r\n')
                           
                
                    
                procCount+=1
                    
        print('written results to '+resultFilename)
            
    
    ###ERROR EVALUATION




    def evaluateError(self, procScores, errorType, ngram):
        """
        if it is an error in manual processing:
        1)if the processor's score is larger than its limit it means the processor thinks
        it's ok==>False Corr
        2)otherwise, the processor thinks it's an error, too ==>  TrueError
        """
        for i in range(0, len(procScores)):
            procTitle = self.procList[i]
            limit = self.getLimit(procTitle, ngram)                
            if procScores[i]>limit:
                if errorType in self.processorFalseCorr[procTitle]:
                    self.processorFalseCorr[procTitle][errorType].append(ngram)
                else:
                    self.processorFalseCorr[procTitle][errorType] = [ngram]
            else:
                if errorType in self.processorTrueError[procTitle]:
                    self.processorTrueError[procTitle][errorType].append(ngram)
                else:
                    self.processorTrueError[procTitle][errorType] = [ngram]




    def evaluateNotError(self, procScores, errorType, ngram):
        """
        if it is correct in manual processing:
        1)if the processor's score is larger than its limit it means the processor thinks
        it's ok==>True Corr
        2)otherwise, the processor thinks it's an error ==>  FalseError
        """
        for i in range(0, len(procScores)):
            procTitle = self.procList[i]                
            if procScores[i] > self.PROC_SCORE_LIM[procTitle]:
                if errorType in self.processorTrueCorr[procTitle]:
                    self.processorTrueCorr[procTitle][errorType].append(ngram)
                else:
                    self.processorTrueCorr[procTitle][errorType] = [ngram]
            else:
                if errorType in self.processorFalseError[procTitle]:
                    self.processorFalseError[procTitle][errorType].append(ngram)
                else:
                    self.processorFalseError[procTitle][errorType] = [ngram]

    ###MEASURES   

    def countPrecision(self, processor, errorType="ALL"):
        if errorType=="ALL":
            
            numOfTrueErrors = 0
            for key, value in self.processorTrueError[processor].iteritems():
                numOfTrueErrors+=len(self.processorTrueError[processor][key])
                
            numOfFalseErrors = 0
            
            for key, value in self.processorFalseError[processor].iteritems():
                numOfFalseErrors+=len(self.processorFalseError[processor][key])
            

            return numOfTrueErrors/float(numOfTrueErrors+numOfFalseErrors)
        
        if errorType in self.processorTrueError[processor]:
            numOfTrueErrors = len(self.processorTrueError[processor][errorType])
        else:
            numOfTrueErrors = 0
            
        if errorType in self.processorFalseError[processor]:
            numOfFalseErrors = len(self.processorFalseError[processor][errorType])
        else:
            numOfFalseErrors = 0
        
        #the error type has not appeared in the dataset
        if float(numOfTrueErrors+numOfFalseErrors) == 0:
            return 1.0
        return numOfTrueErrors/float(numOfTrueErrors+numOfFalseErrors)


    def countRecall(self, processor, errorType="ALL"):
        if errorType=="ALL":
            
            numOfTrueErrors = 0
            for key, value in self.processorTrueError[processor].iteritems():
                numOfTrueErrors+=len(self.processorTrueError[processor][key])
                
            numOfFalseCorr = 0
            
            for key, value in self.processorFalseCorr[processor].iteritems():
                numOfFalseCorr+=len(self.processorFalseCorr[processor][key])
            

            return numOfTrueErrors/float(numOfTrueErrors+numOfFalseCorr)
        
        if errorType in self.processorTrueError[processor]:
            numOfTrueErrors = len(self.processorTrueError[processor][errorType])
        else:
            numOfTrueErrors = 0
            
        if errorType in self.processorFalseCorr[processor]:
            numOfFalseCorr = len(self.processorFalseCorr[processor][errorType])
        else:
            numOfFalseCorr = 0 
        
        #if this error type has not appeared in the dataset      
        if float(numOfTrueErrors+numOfFalseCorr) == 0:
            return 1.0
        
        return numOfTrueErrors/float(numOfTrueErrors+numOfFalseCorr)

    def getTotalNumOfErrors(self, errorType):
        numOfErrors = 0
        if errorType=="ALL":
            for processor in self.procList:
                for errorType, value in self.processorTrueError[processor].iteritems():
                    if errorType in self.ERRORS:
                        numOfErrors+=len(self.processorTrueError[processor][errorType])
                    
                for errorType, value in self.processorFalseCorr[processor].iteritems():
                    if errorType in self.ERRORS:
                        numOfErrors+=len(self.processorFalseCorr[processor][errorType])
        else:
            for processor in self.procList:
                if errorType in self.processorTrueError[processor]:
                    numOfErrors+=len(self.processorTrueError[processor][errorType])
                if errorType in self.processorFalseCorr[processor]:
                    numOfErrors+=len(self.processorFalseCorr[processor][errorType])
        return numOfErrors
    
    
    
    ###UTILS    
    
    
    def isBigramError(self, word1, word2, procTitle):
        self.initCollocationProcessorIfNeeded()
        result, normResult = self.collocationProcessor.getBigramScore(word1, word2, procTitle)
        #print word1+':'+word2+':'+str(result)
        return result <= self.getLimit(procTitle, word1+self.WORD_DELIM+word2)
       

    def getLimit(self, procTitle, ngram):
        """finds the limit for an ngram depending on its pos etc
        """
        ngramParts = ngram.split(self.WORD_DELIM)
        firstWord = ngramParts[0]
        secondWord = ngramParts[1]
        if self.isPreposition(firstWord) or self.isPreposition(secondWord):
            return self.PREP_SCORE_LIM[procTitle]

        elif self.isModalWord(firstWord) or self.isModalWord(secondWord):
            return self.MODAL_SCORE_LIM[procTitle]

        elif self.morph.isAux(firstWord) or self.morph.isAux(secondWord):
            return self.AUX_SCORE_LIM[procTitle]

        return self.PROC_SCORE_LIM[procTitle]

    def isError(self, errorType):
        return not errorType in self.NOT_ERROR_TYPES

    def isPreposition(self, word):
        return word in self.prepList

    def isModalWord(self, word):
        lemmaSet = self.morph.getLemmaSet(word)
        for lemma in lemmaSet:
            if lemma.lower() in self.modalWordList:
                return True
        return False

    def isIgnored(self, errorType):
        return errorType.split('[')[0] in self.IGNORE


    def containsBadSymbols(self, lineParts):
        return self.containsPunct(lineParts) or self.containsLatin(lineParts)

    def containsPunct(self, lineParts):
        for part in lineParts:
            if re.search(ur',|-|–|\.|<|>|«|»|“|”|:|\(|\)|`|\'|\?|/|%|!|;|\+|\d|—',part):
                return True
        return False

    def containsLatin(self, lineParts):
        for part in lineParts:
            if re.search(ur'[A-Z]|[a-z]',part):
                return True
        return False
    
    
    def initEvaluation(self, procList):
        self.initResources()
        self.initStats()
        self.initProcessors(procList)


    def initResources(self):
        with codecs.open(self.PREP_LIST_FILE, 'r', 'utf-8') as fin:
            for line in fin:
                self.prepList.append(line.strip())


        with codecs.open(self.MODAL_LIST_FILE, 'r', 'utf-8') as fin:
            for line in fin:
                self.modalWordList.append(line.strip())
                
    def initStats(self):
        self.numOfFiles = 0
        self.numOfLines = 0
        
    def initProcessors(self, procList):
        """
        initializes the arrays, necessary for comparing resutls, for each processors
        (RusCorpora, Google NGrams etc)
        """
        if not self.processorsInitialized:
            #processorParts=processorLine.strip().split(self.DELIM)
            #self.procList = processorParts[self.NGRAM_ORDER:-1]
            self.procList = procList
            for proc in self.procList:              
                self.processorTrueError[proc] = dict()
                self.processorFalseError[proc]  = dict()
                self.processorTrueCorr[proc]  = dict()
                self.processorFalseCorr[proc]  = dict()
            self.processorsInitialized = True
            
    def initCollocationProcessorIfNeeded(self):
        if not self.collocationProcessor:
            self.collocationProcessor = CollocationProcessor()
                
                
    #CONVERSION UTILS


    def printFolderInNormalForm(self, foldername):

        for filename in os.listdir(foldername):
            self.printFileInNormalForm(foldername+"\\"+filename)


    def printFileInNormalForm(self, filename):
        with codecs.open(filename, 'r', 'utf-8') as fin:
            text = fin.read().replace('\n','\r\n')
            with codecs.open('C:\\formatted\\'+filename.split('\\')[-1], 'w', 'utf-8') as fout:
                fout.write(text)


    def printProcessedFolder(self, foldername):
        for filename in os.listdir(foldername):
            outputFolder = foldername+'_plain'
            if not os.path.exists(outputFolder):
                os.makedirs(outputFolder)
            self.printProcessedFile(foldername+"\\"+filename, outputFolder)

    def printProcessedFile(self, filename, outputFolder):
        with codecs.open(filename, 'r', 'utf-8') as fin:
            outRes=u''
            fin.readline()
            curRes = True
            for line in fin:
                try:
                    lineToOutput, res =  self.getProcessedResult(line)
                    if res!=curRes:
                        if curRes:
                            outRes+=' [['+lineToOutput
                        else:
                            outRes+=' '+lineToOutput+']]'
                    else:
                        outRes+=' '+lineToOutput
                    curRes = res
                except Exception, e:
                    print line +'_'+filename+':'+str(e)
            with codecs.open(outputFolder+'\\'+filename.split('\\')[-1], 'w', 'utf-8') as fout:
                fout.write(outRes)

    def getProcessedResult(self, line):
        lineParts = line.strip().split('\t')
        googleScore = int(lineParts[3])
        if not self.containsBadSymbols([lineParts[0],lineParts[1]]):
            limit = self.getLimit('google', lineParts[0]+' '+lineParts[1])
            if googleScore <= limit:
                return lineParts[0], False
        return lineParts[0], True
    
    
    def reprocessEvaluatedFolder(self, foldername, procChanged):
        newSubFolder = 'changed_'+ time.strftime("%Y%m%d")
        resultFoldername = os.path.join(foldername, newSubFolder)
        if not os.path.exists(resultFoldername):
            os.makedirs(resultFoldername)
        for filename in os.listdir(foldername):
            oldFilename = os.path.join(foldername, filename)
            if os.path.isfile(oldFilename):
                newFilename = os.path.join(resultFoldername, filename)
                print newFilename
                self.reprocessEvaluatedFile(oldFilename, newFilename, procChanged)   
    
    def reprocessEvaluatedFile(self, filename, newFilename, procChanged):
        self.initCollocationProcessorIfNeeded()
        isFirstLine = True
        procList = []
        with codecs.open(newFilename, 'w', 'utf-8') as fout:
            with codecs.open(filename, 'r', 'utf-8') as fin:
                for line in fin:
                    if u'негритянкой' in line:
                        aaaa=9
                    
                    
                    lineParts = line.split(self.DELIM)
                    if isFirstLine:
                        isFirstLine = False
                        #processors' names
                        for i in range(2, len(lineParts) - 1):
                            procList.append(lineParts[i].strip())
                        fout.write(line)
                    else:
                        results = []
                        for i, procTitle in enumerate(procList):
                            if procTitle == procChanged:
                                score,normScore = self.collocationProcessor.getBigramScore(lineParts[0], lineParts[1], procTitle)
                                results.append(str(score))
                            else:
                                results.append(lineParts[2 + i])
                        totalLineParts = [lineParts[0], lineParts[1]] + results + [lineParts[-1]]
                        
                        fout.write(self.DELIM.join(totalLineParts))        
                

"""def main():
    if len(sys.argv)<2:
        print("usage: python ResultEvaluator.py inputFoldername")
        return
    ResultEvaluator().evaluateFolder(sys.argv[1], ["rc", "google"])
    
    
if __name__ == "__main__":
    main()"""





"""resultEvaluator=ResultEvaluator()

resultEvaluator.reprocessEvaluatedFolder(
                                       "D:\\Heritage\\HeritageManualProcessing\\evaluated\\20130604_evaluated",
                                      'google')"""
#resultEvaluator.printFolderInNormalForm("E:\\LingM\\Heritage\\20130415_Processed\\20130505-res-NoPunct\\evaluated\\")
#resultEvaluator.printFolderInNormalForm("E:\\LingM\\Heritage\\20130415_Processed\\20130420-res-Anna\\evaluated\\bi\\")
#resultEvaluator.printFolderInNormalForm("E:\\LingM\\Heritage\\20130415_Processed\\20130507_resHeuristics\\evaluated\\")
#resultEvaluator.initResources()

#print resultEvaluator.isModalWord(u'может')





#ResultEvaluator().evaluateFolder("D://Heritage/processingResult/20131216_ToAnnotate/processed/evaluated/changed_20131220/", ['google', 'googleNormalized'])
#ResultEvaluator().reprocessEvaluatedFolder("D://Heritage/processingResult/20131216_ToAnnotate/processed/evaluated/", 'google')
#ResultEvaluator().printProcessedFolder("E:\\LingM\\Heritage\\20130415_Processed\\20130505-res-NoPunct\\evaluated\\")
#ResultEvaluator().printProcessedFolder("E:\\LingM\\Heritage\\20130415_Processed\\20130507_resHeuristics\\evaluated\\")


#ResultEvaluator().evaluateFolder("D://Heritage/HeritageManualProcessing/evaluated/20130604_evaluated/", ['rc', 'google'])
#ResultEvaluator().evaluateFolder("D://Heritage/HeritageManualProcessing/evaluated/20130906_evaluated/", ['rc', 'google'])
#ResultEvaluator().evaluateFolder("E:\\LingM\\Heritage\\20130415_Processed\\20130420-res-Anna\\evaluated\\bi\\")
#ResultEvaluator().evaluateFolder("E:\\LingM\\Heritage\\20130415_Processed\\20130507_resHeuristics\\evaluated\\")