# coding=utf-8
__author__ = 'gisly'
import codecs, os

import xml.etree.ElementTree as ET


from src.ru.gisly.evaluate_result.ResultEvaluator import ResultEvaluator

resultEvaluator = ResultEvaluator()

DEFAUT_PROCESSOR = 'google'
CURRENT_PART_NUM = 5

assessorGoodProcError = []
assesorErrorProcGood =[]
assessorBigramsTotalNum = 0
assessorErrorProcError = 0


def compareManualAnnotationsFolders(folder1, folder2):
    totalCount = 0
    errorCount = 0
    an1IsError = 0
    an2IsError = 0

    for filename1Short in os.listdir(folder1):
        print filename1Short
        filename1 = os.path.join(folder1, filename1Short)
        filename2 = os.path.join(folder2, filename1Short)
        fileTotalCount, fileErrorCount, fileAn1IsError, fileAn2IsError = \
                                compareManualAnnotationsFiles(filename1, filename2)
        totalCount+=fileTotalCount
        errorCount+=fileErrorCount
        an1IsError+=fileAn1IsError
        an2IsError+=fileAn2IsError
    PrA = errorCount/float(totalCount)
    print("an1ErrRate="+str((an1IsError)/float(totalCount)))
    print("an2ErrRate="+str((an2IsError)/float(totalCount)))
    an1an2BothError = (an1IsError/float(totalCount))*(an2IsError/float(totalCount))
    an1CorrRate = (totalCount - an1IsError)/float(totalCount)
    an2CorrRate = (totalCount - an2IsError)/float(totalCount)
    print("an1CorrRate="+str(an1CorrRate))
    print("an2CorrRate="+str(an2CorrRate))
    an1an2BothCorr = ((totalCount - an1IsError)/float(totalCount))*((totalCount - an2IsError)/float(totalCount))
    PrE = an1an2BothError + an1an2BothCorr
    
    print(str(PrA))
    print(str(PrE))
    print str(countKappa(PrA, PrE))

def countKappa(PrA, PrE):   
    return (PrA - PrE)/(1 - PrE)
        
def compareManualAnnotationsFiles(filename1, filename2):
    totalCount = 0
    notEqualCount = 0
    an1IsError = 0
    an2IsError = 0

    
    with codecs.open(filename1, 'r', 'utf-8') as fin1:
        with codecs.open(filename2, 'r', 'utf-8') as fin2:
            for line1, line2 in zip(fin1, fin2):
                if 'google' in line1:
                    continue
                totalCount+=1
                errorType1 = getErrorType(line1)
                errorType2 = getErrorType(line2)
                isError1 = isError(errorType1)
                isError2 = isError(errorType2)
                if isError1:
                    an1IsError+=1

                if isError2:
                    an2IsError+=1

                if isError1 != isError2:
                    print line1+'!='+line2
                    notEqualCount+=1
    return totalCount, notEqualCount, an1IsError, an2IsError

def isError(errorType):
    return errorType!='-1'
                    
def getErrorType(line):
    lineParts = line.strip().split('\t')
    if len(lineParts)<CURRENT_PART_NUM:
        return '-1'
    return lineParts[-1]


def checkAnnotationsFromFolder(foldername):
    for filename in os.listdir(foldername):
        try:
            checkAnnotationsFromFile(os.path.join(foldername, filename))
        except Exception, e:
            print filename + ':'+str(e)
    
    correctNum = assessorBigramsTotalNum - len(assessorGoodProcError) - len(assesorErrorProcGood)
    precision = assessorErrorProcError/float(assessorErrorProcError + len(assessorGoodProcError))
    recall = assessorErrorProcError/float(assessorErrorProcError + len(assesorErrorProcGood))
    fScore = 2*precision*recall/float(precision + recall)
    print('correct rate:'+str(correctNum/float(assessorBigramsTotalNum)))
    print('precision:'+str(precision))
    print('recall:'+str(recall))
    print('f-score:'+str(fScore))


def checkAnnotationsFromFile(filename):
    root=ET.fromstring(getAnnotatedFileText(filename))
    sentences=root.findall('.//se')
    for sentence in sentences:
        fragments = sentence.findall('.//fragment')
        checkFragments(fragments)
        
    correctNum = assessorBigramsTotalNum - len(assessorGoodProcError) - len(assesorErrorProcGood)
    print('correct rate:'+str(correctNum/float(assessorBigramsTotalNum)))
        
    print('assessorGoodProcError:'+str(len(assessorGoodProcError)))
    """for ngram in assessorGoodProcError:
        print ngram[0]+' '+ngram[1]"""
        
    print('assesorErrorProcGood:'+str(len(assesorErrorProcGood)))
    """for ngram in assesorErrorProcGood:
        print ngram[0]+' '+ngram[1]"""
        
        
###TODO: Тире может быть в отдельном фрагменте — учесть!!!
### не проверять, если после слова дефис ?

###убирать лишние символы!
        
def checkFragments(fragments):
    prevWordPre = None
    prevWordPost = None
    isPrevAssessorError = None
    for i in range(0, len(fragments), 2):
        curFullWordsPre = getWordsFromFragment(fragments[i])
        curFullWordsPost = getWordsFromFragment(fragments[i+1])
        

        prevWordPre, prevWordPost, isPrevAssessorError = checkFragmentWords(prevWordPre, prevWordPost, 
                                                                            isPrevAssessorError, 
                                                                            curFullWordsPre, curFullWordsPost)

            
            
def checkFragmentWords(prevWordPre, prevWordPost, isPrevAssessorError, wordsPre, wordsPost):
    global assessorBigramsTotalNum
    global assessorErrorProcError
    #if the fragment is empty
    if not wordsPre:
        return None, None, False
    
    for i in range(0, len(wordsPre)):
        
        curFullWordPre = wordsPre[i]
        curWordPre = getAnaFromWord(curFullWordPre)
        #print curWordPre
        
        if i < len(wordsPost):
            curFullWordPost = wordsPost[i]
            curWordPost = getAnaFromWord(curFullWordPost)
        else:
            curWordPost = 'EMPTY'

        
        isCurAssesorError = isErrorAssessor(curWordPre, curWordPost)
        if prevWordPre and prevWordPost:
            assessorIsError =  isPrevAssessorError or isCurAssesorError 
            assessorBigramsTotalNum +=1
            processorIsError = resultEvaluator.isBigramError(prevWordPre, curWordPre, DEFAUT_PROCESSOR)
            if assessorIsError and not processorIsError:
                assesorErrorProcGood.append([prevWordPre, curWordPre])
            elif not assessorIsError and processorIsError:
                assessorGoodProcError.append([prevWordPre, curWordPre])
            elif assessorIsError and processorIsError:
                assessorErrorProcError+=1

                
        if curFullWordPre is not None and curFullWordPre.tail and curFullWordPre.tail.strip()!='':
            prevWordPre = None
            prevWordPost = None
            isPrevAssessorError = None
        else:
            prevWordPre = curWordPre
            prevWordPost = curWordPost
            isPrevAssessorError = isCurAssesorError
            
    return prevWordPre, prevWordPost, isPrevAssessorError

                

def getWordsFromFragment(fragment):
    return fragment.findall('.//w')


def getAnaFromWord(word):
    if word is None:
        return 'EMPTY'
    return word.findall('.//ana')[-1].tail

def getAnnotatedFileText(filename):
    with codecs.open(filename, 'r', 'utf-8') as fin:
        return fin.read()
            
            
            
###the assessor types the same word if the word is correct
###and a correction if the word is wrong            
def isErrorAssessor(word1, word2):
    return word1!=word2
            
            
#checkAnnotationsFromFile("D://Heritage/annotatedTexts/Max_HL_AM_11-12_W_22_1_p_annotated_bibliography_NT-0.xhtml")
#checkAnnotationsFromFile("D://Heritage/annotatedTexts/Lia_FL_AM_11-12_W_2_2_p_plus_sum_NT-0.xhtml")   
checkAnnotationsFromFolder("D://Heritage/annotatedTexts/comparison/documents-export-2013-12-20")
#checkAnnotationsFromFile("D://Heritage/annotatedTexts/comparison/Anna_HL_AL_10-11_W_10_2_p_plus_pph_T.xhtml")

#compareManualAnnotationsFolders("D://Heritage/_MILESTONES/20131219_TODO/Lena", "D://Heritage/_MILESTONES/20131219_TODO/Natasha")     
        