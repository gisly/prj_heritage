__author__ = 'gisly'
import codecs
import time
import datetime


class HeritageLogger(object):
    #a file for logging
    logFile=None
    def __init__(self,logFileName):
        self.logFile=codecs.open(logFileName,'a','utf-8')

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        if self.logFile is not None:
            self.logFile.close()

    def logToFileConsole(self, message, isToFileLog=True, isToConsoleLog=True):
        message=self.formatMessage(message)
        if isToFileLog:
            self.logFile.write(message+'\r\n')
        if isToConsoleLog:
            print(message)

    def formatMessage(self, message):
        return str(datetime.datetime.now()) +'\t'+message
