# coding=utf-8
__author__ = 'gisly'


import sys,os.path
srcFolder = os.path.dirname(__file__)
gislyFolder = os.path.dirname(os.path.abspath(srcFolder))
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
syntaxChecking = os.path.dirname(os.path.abspath(ruFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(syntaxChecking)))
sys.path.append(parentFolder)
from src.ru.gisly.syntaxChecking.syntaxUtils import getGrammarInfo, getSortedSignificantGrammarList, isPreposition, isNoun, isSPRO, getCase, agreeAdjNounNoHom, \
                                                    isFullAdj, getLemma, isFollowedByPunctMark, isGovernableByPrep,\
                                                    getSortedSignificantGrammarListHomonymy

import codecs
import math
from src.ru.gisly.CorpusDb import CorpusDb
import xml.etree.ElementTree as ET


from src.ru.gisly.lemmer import Lemmer


stats=None
db=None


class MorphoStatistics(object):
    
    morphUnigrams = None
    #we store very rare ngrams together with the file names so that we can easily check
    #if it's not a mistake 
    morphNgrams = None
    morphNgramsFilenames = None
    
    morphTrigrams = None
    morphTrigramsFilenames = None
    
    
    db=CorpusDb()
    lemmer = Lemmer()
    
    
    MIN_LIMIT = 5
    NGRAM_DELIM = '\t'
    
    #the minimal number of cases in the ngram db
    
    MIN_COUNT = 10
    MIN_PROB_COUNT = -11
    
    
    def initDB(self):
        #self.db=CorpusDB()
        pass
    
    
    def processRuscorporaMorphNgramStats(self, folder, methodToCall):
        self.initDB()
        
        
        self.morphNgrams = dict()
        self.morphNgramsFilenames = dict()
        
        self.morphUnigrams = dict()
        
        self.morphTrigrams = dict()
        self.morphTrigramsFilenames = dict()
        
        
        numOfFiles=0
        for root, dirs, files in os.walk(folder):
            for name in files:
                self.processMorphNgramStatsFile(os.path.join(root, name), 
                                                    methodToCall)
                numOfFiles+=1
                if numOfFiles%20==0:
                    print 'processed:'+str(numOfFiles) 
                    
        with codecs.open('tempStats.txt','w','utf-8') as fout:
            for key, value in self.morphNgrams.iteritems():
                fout.write(key+'\t'+str(value))
                if key in self.morphNgramsFilenames:
                    fout.write('\t'+self.morphNgramsFilenames[key])
                fout.write('\r\n')
                
                
        with codecs.open('trigramStats.txt','w','utf-8') as fout:
            for key, value in self.morphTrigrams.iteritems():
                fout.write(key+'\t'+str(value))
                if key in self.morphTrigramsFilenames:
                    fout.write('\t'+self.morphTrigramsFilenames[key])
                fout.write('\r\n')
                
        with codecs.open('unigramTempStats.txt','w','utf-8') as fout:
            for key, value in self.morphUnigrams.iteritems():
                fout.write(key+'\t'+str(value))
                fout.write('\r\n')
                
        with codecs.open('bigramTempLogProbs.txt','w','utf-8') as fout:
            for key, bigramvalue in self.morphNgrams.iteritems():
                unigramvalue = self.morphUnigrams[key.split('\t')[0]]
                logProb  = math.log((float(bigramvalue)/unigramvalue))
                fout.write(key+'\t'+str(logProb))
                fout.write('\r\n')
                
    def countRuscorporaMorphNgramStats(self, folder):
        self.processRuscorporaMorphNgramStats(folder, self.countRuscorporaMorphNgramStatsWordForms)
        
    def checkRuscorporaMorphNgramStats(self, folder):
        self.processRuscorporaMorphNgramStats(folder, self.checkRuscorporaMorphNgramStatsWordForms)
        
        
    def checkMorphNgramsPlainText(self, filename):
        mystemText =   self.lemmer.tokenizeApplyMystemFile(filename)
        return self.processMorphNgramStatsText(mystemText, self.checkRuscorporaMorphNgramStatsWordForms)
        
    def checkMorphNgramsPlainTextText(self, text):
        mystemText =   self.lemmer.tokenizeApplyMystemText(text)
        return self.processMorphNgramStatsText(mystemText, self.checkRuscorporaMorphNgramStatsWordForms)
                
                    
    
    
    
    def processMorphNgramStatsFile(self, filename, methodToCall):
        result=[]
        try:
            with codecs.open(filename, 'r', 'cp1251') as f:
                text=f.read().encode('utf-8').replace('windows-1251','utf-8')
                root=ET.fromstring(text)
            sentences=root.findall('.//se')
            for sentence in sentences:
                result.append(self.processMorphNgramStatsSentence(sentence, filename, methodToCall))
        except Exception, e:
            print str(e)+ ' in '+filename
        return result
            
    def processMorphNgramStatsText(self, text, methodToCall):
        result=[]
        try:
            text=text.encode('utf-8').replace('windows-1251','utf-8')
            root=ET.fromstring(text)
            sentences=root.findall('.//se')
            for sentence in sentences:
                result.append(self.processMorphNgramStatsSentence(sentence, 'IN_TEXT', methodToCall))
        except Exception, e:
            print str(e)
        return result
            
    def processMorphNgramStatsSentence(self, sentence, filename,methodToCall):
        wordForms=sentence.findall('.//w')
        return methodToCall(wordForms,filename) 
    
    def countRuscorporaMorphNgramStatsWordForms(self, wordForms, filename):
        
       
        prevGram = None
        prevWord = None
        prevPrevGram = None
        prevPrevWord = None
        for i in range(0, len(wordForms)):
            curWord = wordForms[i]
            curGram = ','.join(getSortedSignificantGrammarList(curWord))

            if prevGram:

                morphNgram = self.createMorphNgram(prevGram, curGram, prevWord)
                
                

                """if prevGram.startswith('A,m') and curGram.startswith('S,f') and 'nom' in curGram:
                            print getLemma(prevWord)+' '+getLemma(curWord)"""
                            
                firstPart = morphNgram.split('\t')[0]
                if firstPart in self.morphUnigrams:
                    self.morphUnigrams[firstPart]+=1
                else:
                    self.morphUnigrams[firstPart] = 1
                #we store very rare ngrams together with the file names so that we can easily check
                #if it's not a mistake 
                if morphNgram in self.morphNgrams:
                    self.morphNgrams[morphNgram]+=1
                    if morphNgram in self.morphNgramsFilenames and self.morphNgrams[morphNgram] > self.MIN_LIMIT:
                        del self.morphNgramsFilenames[morphNgram] 
                        
                else:
                    self.morphNgrams[morphNgram] = 1 
                    self.morphNgramsFilenames[morphNgram] = filename+'\t'+getLemma(prevWord)+'\t'+getLemma(curWord)
                
                
                
                if prevPrevGram:
                    morphTrigram = self.createMorphTrigram(prevPrevGram, prevGram, curGram, prevPrevWord, prevWord)
                    
                    if morphTrigram in self.morphTrigrams:
                        self.morphTrigrams[morphTrigram]+=1
                        if morphTrigram in self.morphTrigramsFilenames and self.morphTrigrams[morphTrigram] > self.MIN_LIMIT:
                            del self.morphTrigramsFilenames[morphTrigram] 
                            
                    else:
                        self.morphTrigrams[morphTrigram] = 1 
                        self.morphTrigramsFilenames[morphTrigram] = filename+'\t'+getLemma(prevPrevWord)+'\t'+getLemma(prevWord)+'\t'+getLemma(curWord)
                
                
                    
                
                    
            if not isFollowedByPunctMark(curWord):  
                
                prevPrevGram = prevGram
                prevPrevWord = prevWord  
            
                prevGram = curGram
                prevWord = curWord
                
                
        
            else:
                
                prevPrevGram = None
                prevPrevWord = None
                
                prevGram = None
                prevWord = None
                
                
        return 'ok'
            
    def checkRuscorporaMorphNgramStatsWordForms(self, wordForms, filename):
        

        prevGramList = None
        prevWord = None
        result=[]
        for i in range(0, len(wordForms)):
            curWord = wordForms[i]
            curGramList = getSortedSignificantGrammarListHomonymy(curWord)

            
            if prevGramList:
                isBad = True
                for curGram in curGramList:
                    for prevGram in prevGramList:
                        
                        #TODO: mystem bug (?)
                        '''if 'A-PRO' in prevGram:
                            prevGram='A-PRO'''

                        morphNgram = self.createMorphNgram(prevGram, curGram, prevWord)
              
                        temp = morphNgram.split(self.NGRAM_DELIM)
                        #count = self.db.getRCScoreMorphBigram(temp[0],temp[1])
                        count = self.db.getRCScoreMorphProbBigram(temp[0],temp[1])
                        
                        
                        """print "count="+str(count)
                        print morphNgram+' '+getLemma(prevWord)+' '+getLemma(curWord)+':'+str(count)"""
                        if count >= self.MIN_PROB_COUNT:
                            isBad = False
                            break
                        
                if isBad:
                    print '======================='
                    print 'bad'+morphNgram+' '+getLemma(prevWord)+' '+getLemma(curWord)+':'+str(count)
                    result.append(getLemma(prevWord)+' '+getLemma(curWord))
                        
                #check ngram
                
            if not isFollowedByPunctMark(curWord):    
            
                prevGramList = curGramList
                prevWord = curWord
        
            else:
                prevGramList = None
                prevWord = None
                
        return result
                
        
    def createMorphNgram(self, prevGram, curGram, prevWord):
        if isPreposition(prevGram) and isGovernableByPrep(curGram):
            return self.composeMorphNgram([getLemma(prevWord), getCase(curGram)])
        return self.composeMorphNgram([prevGram, curGram])  
    
    
    def createMorphTrigram(self, prevPrevGram, prevGram, curGram, prevPrevWord, prevWord):
        if isPreposition(prevPrevGram) and isGovernableByPrep(prevGram):
            return self.composeMorphNgram([getLemma(prevPrevWord), getCase(prevGram), curGram])
        if isPreposition(prevGram) and isGovernableByPrep(curGram):
            return self.composeMorphNgram([prevPrevGram, getLemma(prevWord), getCase(curGram)])
        return self.composeMorphNgram([prevPrevGram, prevGram, curGram]) 
                       
             
            
    def composeMorphNgram(self, partList):
        return self.NGRAM_DELIM.join(partList)        
            

    def getPrepGovernmentStatsCorpus(self, folder):
        self.initDB()
        numOfFiles=0
        for root, dirs, files in os.walk(folder):
            for name in files:
                self.getPrepGovernmentStatsFile(os.path.join(root, name))
                numOfFiles+=1
                if numOfFiles%20==0:
                    print 'processed:'+str(numOfFiles)


    def getPrepGovernmentStatsFile(self, filename):
        try:
            with codecs.open(filename, 'r', 'cp1251') as f:
                text=f.read().encode('utf-8').replace('windows-1251','utf-8')
                root=ET.fromstring(text)
            sentences=root.findall('.//se')
            for sentence in sentences:
                self.getPrepGovernmentStatSentence(sentence, filename)
        except Exception, e:
            print str(e)+' in '+filename

    def getPrepGovernmentStatSentence(self, sentence,filename):
        wordForms=sentence.findall('.//w')
        self.getPrepGovernmentStatsWordForms(wordForms,filename)

    def getPrepGovernmentStatsWordForms(self, wordForms, fileName):
        for i in range(1, len(wordForms)):
            prev=wordForms[i-1]
            cur=wordForms[i]
            prevGrammarInfo= getGrammarInfo(prev)[0]
            if isPreposition(prevGrammarInfo):
                curGrammarInfo=getGrammarInfo(cur)[0]
                if isNoun(curGrammarInfo) or isSPRO(curGrammarInfo):
                    case=getCase(curGrammarInfo)
                    prepWord=wordForms[i-1].find('ana').attrib['lex']
                    if case=='acc' and prepWord == u'с':
                        print fileName+':'+prepWord+':'+wordForms[i].find('ana').attrib['lex']
                        #self.db.insertPrepositionCase(prepWord,case)







    def getAgrStatsCorpus(self, folder):
        self.stats=dict()
        self.stats['total']=0
        self.stats['na']=0
        self.stats['bad']=u''
        for root, dirs, files in os.walk(folder):
            for name in files:
                self.getAgrStatsFile(os.path.join(root, name))


        '''with codecs.open('C:\\res','a', 'utf-8') as fout:
            fout.write(str(self.stats['total']))
            fout.write(str(self.stats['na']))
            fout.write(str(self.stats['bad']))'''

        print self.stats['total']

    def getAgrStatsFile(self, filename):
        try:
            with codecs.open(filename, 'r', 'cp1251') as f:
                text=f.read().encode('utf-8').replace('windows-1251','utf-8')
                root=ET.fromstring(text)
            sentences=root.findall('.//se')
            for sentence in sentences:
                self.getAgrStatsSentence(sentence, filename)
        except Exception, e:
            print str(e)
    def getAgrStatsSentence(self, sentence,filename):
        wordForms=sentence.findall('.//w')
        self.getAgrStatsWordForms(wordForms,filename)
    def getAgrStatsWordForms(self, wordForms, fileName):
        for i in range(1, len(wordForms)):
            prev=wordForms[i-1]
            cur=wordForms[i]
            prevGrammarInfo=getGrammarInfo(prev)[0]
            if isFullAdj(prevGrammarInfo):
                curGrammarInfo=getGrammarInfo(cur)[0]
                if isNoun(curGrammarInfo):
                    self.stats['total']+=1
                    if not agreeAdjNounNoHom(prevGrammarInfo, curGrammarInfo):
                        prevWordLex=wordForms[i-1].find('ana').attrib['lex']
                        curWordLex=wordForms[i].find('ana').attrib['lex']
                        print fileName
                        print str(i)
                        #self.stats['na']+=1
                        '''self.stats['bad']+=prevWordLex+' '+curWordLex+' '+fileName+'\n'''
                        with codecs.open('E:\\LingM\\ruscorpora_Texts\\results\\res.txt','a', 'utf-8') as fout:
                            fout.write(prevGrammarInfo+','+curGrammarInfo+'\n')
                            fout.write(prevWordLex+' '+curWordLex+' '+fileName+'\n')
                    

#MorphoStatistics().countRuscorporaMorphNgramStats('D://Heritage/ruscorpora/texts')
result=MorphoStatistics().checkMorphNgramsPlainText('D://Heritage/toTestMorphology/text1.txt')
"""for res in result:
    if res:
        for element in res:
            print element"""
"""result=MorphoStatistics().checkMorphNgramsPlainTextText(u'Ты как пери из райского сада')
for res in result:
    if res:
        for element in res:
            print element"""