# coding=utf-8
__author__ = 'gisly'
import codecs
class GrammarStatisitcs(object):
    prepStatsFile='C:\\Users\\user\\IdeaProjects\\'\
                'HeritageProject\\PythonModule\\resources\\stats\\prepositions.csv'

    DELIM=','
    LIMIT = 40

    def __init__(self):
        #self.loadPrepStats()
        pass

    def loadPrepStats(self):
        self.prepStats=dict()
        with codecs.open(self.prepStatsFile, 'r', 'utf-8') as fin:
            for line in fin:
                lineParts = line.split(self.DELIM)
                prep = lineParts[0]
                count = int(lineParts[2])
                if count > self.LIMIT:
                    case=lineParts[1]
                    oldCaseList = self.prepStats.get(prep)
                    if oldCaseList:
                        oldCaseList.append(case)
                    else:
                        self.prepStats[prep] = [case]


    def cleanPrepStats(self):
        newFile =     self.prepStatsFile+'_new.txt'
        with codecs.open(self.prepStatsFile, 'r', 'utf-8') as fin:
            with codecs.open(newFile, 'w', 'utf-8') as fout:
                for line in fin:
                    if not ',nom,' in line:
                        fout.write(line)




    def getPrepStats(self):
        return self.prepStats

    def getPrepCases(self, prepWord):
        return self.prepStats.get(prepWord)


#GrammarStatisitcs().cleanPrepStats()
