# coding=utf-8
__author__ = 'gisly'
import sys,os.path
#testFolder = os.path.dirname(__file__)
#gislyFolder=os.path.dirname(os.path.abspath(testFolder))
srcFolder = os.path.dirname(__file__)
gislyFolder = os.path.dirname(os.path.abspath(srcFolder))
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
syntaxChecking = os.path.dirname(os.path.abspath(ruFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(syntaxChecking)))
sys.path.append(parentFolder)
from src.ru.gisly.syntaxChecking.GrammarStatisitcs import GrammarStatisitcs
from src.ru.gisly.lemmer import Lemmer



lemmer=Lemmer()
grammarStats=GrammarStatisitcs()

ADJECTIVE='A,'
ADJECTIVE_PRO='A-PRO'
SPRO='S-PRO'
PREPOSITION='PR'
NOUN='S,'
NOUN_PRO='S-PRO'
PARTICIPLE='partcp'
BASTARD='bastard'
FEATURE_DELIMITER=','
NO_HOME_DELIMITER='[,|=]'
NOT_INFL=['anim','inan','plen','norm', 'A', 'A-PRO', 'S', '0', 'obsc']
POS=[ADJECTIVE,ADJECTIVE_PRO,NOUN]
GENDER='f, m, n'
PLURAL='pl'

NON_SIGNIFICANT=['famn', 'patrn', 'persn', 'zoon', 'obsc','abbr','distort', 'anim', 'inan', 'ciph', 'norm', '0',
                 'bastard']




MYSTEM_VARIANTS = {'SPRO':'S-PRO', 'APRO':'A-PRO', 'ADVPRO':'ADV', 'ADV,PRAEDIC':'PRAEDIC', 'supr':'plen'}


def applyMystem(textFile):
    return lemmer.tokenizeApplyMystemFileReturnFile(textFile)
    
def applyMystemText(text):
    return lemmer.tokenizeApplyMystemTextReturnFile(text)
    
PUNCT_LIST=[',.()']

def getSortedSignificantGrammarList(word):
    return sorted(removeNonSignificantFromGrammarInfo(getGrammarInfoList(word)))

def getSortedSignificantGrammarListHomonymy(word):
    gramInfoList = getGrammarInfo(word)
    return set([','.join(sorted(removeNonSignificantFromGrammarInfo(getNormalizedGramInfoPart(gramInfo))))
                 for gramInfo in gramInfoList])


def getGrammarInfoList(word):
    gramInfo = getGrammarInfo(word)[0]
    return getNormalizedGramInfoPart(gramInfo)


    
def getNormalizedGramInfoPart(gramInfo):
    normalizedGramInfo = normalizeGramInfo(gramInfo)
    return normalizedGramInfo.replace('=',',').split(',')


def normalizeGramInfo(gramInfo):
    res = gramInfo
    for key, value in MYSTEM_VARIANTS.iteritems():
        res = res.replace(key, value)
    return res

def getGrammarInfo(word):
    return [ana.attrib['gr'] for ana in word.findall('./ana')]



def removeNonSignificantFromGrammarInfo(gramList):
    return [gramAttr for gramAttr in gramList if not gramAttr in NON_SIGNIFICANT]
    




def getLemma(word):
    return removeAccents(word.findall('./ana')[-1].tail)

def removeAccents(text):
    if text is None:
        return ''
    return text.replace(u'`','').replace(u'́','').lower()

def normalizeDelimiters(variant):
    return variant.replace('=',',')

def getCase(grammarInfo):
    
    if 'gen2' in grammarInfo:
        return 'gen2'
    if 'gen' in grammarInfo:
        return 'gen'
    if 'dat' in grammarInfo:
        return 'dat'
    if 'acc2' in grammarInfo:
        return 'acc2'
    if 'acc' in grammarInfo:
        return 'acc'
    if 'ins' in grammarInfo:
        return 'ins'
    if 'loc2' in grammarInfo:
        return 'loc2'
    if 'loc' in grammarInfo:
        return 'loc'
    if 'voc' in grammarInfo:
        return 'voc'
    if 'nom' in grammarInfo:
        return 'nom'
    return grammarInfo


def wrongGoverning(prevGrammarInfo,curGrammarInfo, prevWord):
    return not existsAgreement(prevGrammarInfo,curGrammarInfo)\
             or \
            not wrongPrepGoverning(prevGrammarInfo,curGrammarInfo, prevWord)

def existsAgreement(prevGrammarInfo,curGrammarInfo):
    #adj + noun
    adjVariants=getAllAdjVariants(prevGrammarInfo)
    if not adjVariants:
        return True
    nounVariants=getAllNounVariants(curGrammarInfo)
    if not nounVariants:
        return True
    for adjVariant in adjVariants:
        for nounVariant in nounVariants:
            if agreeAdjNoun(adjVariant, nounVariant):
                return True
    return False

def wrongPrepGoverning(prevGrammarInfo,curGrammarInfo, prevWord):
    prepVariants = getAllPrepVariants(prevGrammarInfo)
    if not prepVariants:
        return True
    variants=getAllNounVariants(curGrammarInfo) + getAllAdjVariants(curGrammarInfo)
    if not variants:
        return True
    for variant in variants:
        if governsPrepNoun(variant, prevWord):
            return True
    '''for prevVar in prevGrammarInfo:
        for curVar in curGrammarInfo:
            if isPreposition(prevVar):
                #curGrammarInfo=getGrammarInfo(curGrammarInfo)[0]
                if isNoun(curGrammarInfo) or isSPRO(curGrammarInfo):
                    case=getCase(curGrammarInfo)
                    prepWord=wordForms[i-1].find('ana').attrib['lex']'''




def agreeAdjNoun(adjVariant,nounVariant):
    return nounVariant==adjVariant or adjVariant in nounVariant

def governsPrepNoun(nounVariant, prevWord):
    case=getCase(nounVariant)
    prepWord=prevWord.find('ana').attrib['lex']
    prepCases = grammarStats.getPrepCases(prepWord)
    if prepCases:
        return case in prepCases
    return True


def agreeAdjNounNoHom(adjInfo,nounInfo):
    adjInfFeatures=''.join(getInflectionalFeaturesNoHom(adjInfo))
    nounInfFeatures=''.join(getInflectionalFeaturesNoHom(nounInfo))
    return adjInfFeatures in nounInfFeatures



def getAllAdjVariants(grammarInfo):
    adjVariants=[]
    for variant in grammarInfo:
        if isFullAdj(variant) and not isBastard(variant):
            inflFeatures =getInflectionalFeatures(variant)
            if inflFeatures:
                adjVariants.append(inflFeatures)
    return adjVariants

def getAllNounVariants(grammarInfo):
    nounVariants=[]
    for variant in grammarInfo:
        if (isNoun(variant) or isSProNoun(variant)) and not isBastard(variant):
            nounVariants.append(getInflectionalFeatures(variant))
    return nounVariants


def getAllPrepVariants(grammarInfo):
    prepVariants=[]
    for variant in grammarInfo:
        allFeatures=variant.split(FEATURE_DELIMITER)
        if isPreposition(allFeatures[0]) and not isBastard(allFeatures[0]):
            prepVariants.append(allFeatures[1])
    return prepVariants




def isFullAdj(variant):
    return (variant.startswith(ADJECTIVE) and not ('brev' in variant or 'anom' in variant)) or variant.startswith(ADJECTIVE_PRO)

def isNoun(variant):
    return variant.startswith(NOUN)

def isSProNoun(variant):
    return SPRO in variant


def isSPRO(variant):
    return SPRO in variant

def isFullParticiple(variant):
    return PARTICIPLE in variant and not 'brev' in variant

def isPreposition(variant):
    return variant==PREPOSITION

def isBastard(variant):
    return BASTARD in variant

def isPlural(featureList):
    return PLURAL in featureList


def isGovernableByPrep(gramInfo):
    return isNoun(gramInfo) or isSProNoun(gramInfo) or isFullAdj(gramInfo) or isFullParticiple(gramInfo)

def getInflectionalFeatures(variant):
    allFeatures=variant.split(FEATURE_DELIMITER)
    notInfFeatures= [feature for feature in allFeatures if isNotInfl(feature)]
    if isPlural(notInfFeatures):
        notInfFeatures = [feature for feature in notInfFeatures if not feature in GENDER]
    return notInfFeatures

def getInflectionalFeaturesNoHom(variant):
    allFeatures=normalizeDelimiters(variant).split(FEATURE_DELIMITER)
    notInfFeatures= [feature for feature in allFeatures if isNotInfl(feature)]
    '''if isPlural(notInfFeatures):
notInfFeatures = [feature for feature in notInfFeatures if not feature in GENDER]'''
    return notInfFeatures

def isNotInfl(feature):
    return not feature in NOT_INFL


def isFollowedByPunctMark(wordForm):
    possPunctMark = wordForm.tail
    return (possPunctMark and possPunctMark.strip()!='')


