# coding=utf-8
__author__ = 'gisly'
from src.ru.gisly.syntaxChecking.syntaxUtils import applyMystem, applyMystemText, getGrammarInfo, existsAgreement, wrongGoverning
import xml.etree.ElementTree as ET




class MorphoVerifier(object):

    #utf8_parser = ET.XMLParser(encoding='utf-8')


    def verifyFile(self, fileName):
        newFilename=applyMystem(fileName)
        return self.checkText(newFilename)
    
    def verifyText(self, text):
        newFilename=applyMystemText(text)
        return self.checkText(newFilename)

    def checkText(self, mystemFile):
        tree = ET.parse(mystemFile)
        #root = ET.fromstring(mystemFile.encode('utf-8'), parser = self.utf8_parser)
        root = tree.getroot()
        sentences=root.findall('.//se')
        badPairs=[]
        for sentence in sentences:
            wordForms=sentence.findall('.//w')
            newBadIndices= self.checkWordForms(wordForms)

            for index in newBadIndices:
                badPairs.append(self.getLemmaFromWordForm(wordForms[index-1])+" "+
                                            self.getLemmaFromWordForm(wordForms[index]))

        return badPairs

    def getLemmaFromWordForm(self, wordForm):
        return wordForm.findall('ana')[-1].tail

    def checkWordForms(self,wordForms):
        badPairIndex=[]
        for i in range(1, len(wordForms)):
            prev=wordForms[i-1]
            cur=wordForms[i]
            prevGrammarInfo=getGrammarInfo(prev)
            curGrammarInfo=getGrammarInfo(cur)
            if wrongGoverning(prevGrammarInfo,curGrammarInfo, prev):
                badPairIndex.append(i)
        return badPairIndex




print MorphoVerifier().verifyFile('C:\\Users\\user\\IdeaProjects\\HeritageProject\\'
'PythonModule\\resources\\test.txt')




















