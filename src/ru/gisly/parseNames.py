# coding=utf-8
__author__ = 'gisly'
import codecs

from src.ru.gisly.CorpusDb import CorpusDb


corpusDb = CorpusDb()

        
        
def parseNameFile(filename, nameType):
    with codecs.open(filename, 'r', 'utf-8') as fin:
        for line in fin:
            name = line.strip()
            if len(name)>1:
                corpusDb.insertName({'name':name.upper(), 'type':nameType})
                
                
                
parseNameFile("D:\\LingM\\Dictionaries\\names\\names\\names_f.txt", "f")
parseNameFile("D:\\LingM\\Dictionaries\\names\\names\\names_m.txt", "m")
            