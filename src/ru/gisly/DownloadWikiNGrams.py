# coding=utf-8

__author__ = 'gisly'

import codecs
import os
import re
import sys

import sys,os.path
srcFolder = os.path.dirname(__file__)
gislyFolder = os.path.dirname(os.path.abspath(srcFolder))
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(ruFolder)))
sys.path.append(parentFolder)
from src.ru.gisly.CorpusDb import CorpusDb
from src.ru.gisly.lemmer import Tokenizer


class WikiNGramExtractor(object):
    bigrams = dict()

    corpusDB=CorpusDb()

    tokenizer= Tokenizer()

    DOC_END = '</doc>'
    DOC_START_DELIM = '>'



    USE_PUNCTUATION = False

    PUNCT_PATTERN = ur',|-|–|\.|<|>|«|»|“|”|"|:|\(|\)|`|\'|\?|/|%|!|;|\+|,``|—|\s'

    PUNCT_PATTERN_RETAIN = ur'('+PUNCT_PATTERN+ur')'
    PUNCT_LIST = ['.','!','?',',',':',';','<','>','(',')',u'«',u'»',u'“',u'”', u'—', u'``',u'\\']


    def extractNGramsFromFolder(self, folder):

        for root, dirs, files in os.walk(folder):
            for filename in files:

                self.extractNGramsFromFile(os.path.join(root,filename))
                print os.path.join(root,filename)



    def extractNGramsFromFile(self, filename):
        """
        processes an xml-file in the following format:
        <doc>(plain text with no markup)</doc><doc>.....
        """
        with codecs.open(filename, 'r', 'utf-8') as fin:
            text = fin.read()
            self.processText(text, filename)




    def processText(self, text, filename):
        docTexts = text.split(self.DOC_END)
        for docText in docTexts:
            self.processDocument(docText, filename)
            
    """def processDocument(self, docText, filename):
        
            docTextNoTitle = docText.split(self.DOC_START_DELIM)[-1].strip()
            docTextNoTitle = self.makeReplacements(docTextNoTitle)
            tokens = self.tokenize(docTextNoTitle)
            bigramDict = dict()
            if tokens:
               
                prevWord = self.normalizeWord(tokens[0])
                for i in range(1,len(tokens)):
                    curWord = self.normalizeWord(tokens[i])
                    bigramKey = prevWord + '###'+curWord
                    
                    if bigramKey in bigramDict:
                        bigramDict[bigramKey]+=1
                    else:
                        bigramDict[bigramKey]=1
                    prevWord = curWord
            for key, count in bigramDict.iteritems():
                [ngram0, ngram1] = key.split('###')
                
                bigram = {'ngram0':ngram0, 'ngram1':ngram1, 'count':count}
                self.corpusDB.insertWikibigramDict(bigram, filename)"""

    def processDocument(self, docText, filename):
        
        docTextNoTitle = docText.split(self.DOC_START_DELIM)[-1].strip()
        docTextNoTitle = self.makeReplacements(docTextNoTitle)
        tokens = self.tokenize(docTextNoTitle)
        if tokens:
            prevWord = self.normalizeWord(tokens[0])
            for i in range(1,len(tokens)):
                curWord = self.normalizeWord(tokens[i])
                self.addBigram(prevWord, curWord, filename)
                prevWord = curWord
                
        



    def addBigram(self, prevWord, curWord, filename):
        '''newKey = prevWord+'_'+curWord
        matchCount = self.bigrams.get(newKey)
        if matchCount is None:
            self.bigrams[newKey]  = 1
        else:
            self.bigrams[newKey]+=1 '''

        self.corpusDB.insertWikibigram(prevWord, curWord, filename)

    def makeReplacements(self, text):
        return text.replace(u'́','')

    def tokenize(self, docText):
        #tokens= self.tokenizer.tokenizeReturnArray(docText)
        if self.USE_PUNCTUATION:
            return re.split(self.PUNCT_PATTERN_RETAIN, docText)
        tokens = re.split(self.PUNCT_PATTERN, docText)
        return [word for word in tokens if word!='']



    def normalizeWord(self, word):

        if not re.match(ur'\d+', word):
            return word.strip()

        lastDigit = word[-1]


        if lastDigit=='1' and (len(word)==1 or word[-2]!='1'):
            return 1

        if lastDigit=='2' and (len(word)==1 or word[-2]!='1'):
            return 2

        return 5


#print WikiNGramExtractor().normalizeWord(u'мама')
WikiNGramExtractor().extractNGramsFromFolder('D://Heritage/wikiNotExtracted')
