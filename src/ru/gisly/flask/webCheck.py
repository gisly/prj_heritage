# coding=utf-8
__author__ = 'gisly'
from flask import Flask, request, render_template

import sys,os.path
srcFolder = os.path.dirname(__file__)
gislyFolder = os.path.dirname(os.path.abspath(srcFolder))
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
flaskFolder = os.path.dirname(os.path.abspath(ruFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(flaskFolder)))
sys.path.append(parentFolder)



from src.ru.gisly.syntaxChecking.morphoStats import MorphoStatistics
from src.ru.gisly.ProcessFile import HeritageFileProcessor
from flask import jsonify
import re
import itertools



app = Flask(__name__)

# configuration
DEBUG = True


#spellchecker = SpellcheckingServer();
morphoStats = MorphoStatistics()
heritageFileProcessor = HeritageFileProcessor()

@app.route("/")
def mainPage():
    return render_template('index.html')

@app.route("/spelling")
def spelling():
    return render_template('spelling_yandex.html')

"""@app.route("/syntax")
def syntax():
    text = request.args.get('text')
    if not text:
        text=''
    return render_template('check_syntax.html',text=text)"""

@app.route("/collocations")
def collocations():
    text = request.args.get('text')
    if not text:
        text=''
    return render_template('check_collocations.html',text=text)

    
    
"""@app.route('/check_syntax')
def check_syntax():
    text = request.args.get('text_to_check')
    resultBySentences = morphoStats.checkMorphNgramsPlainTextText(text)
    flatRes = list(itertools.chain.from_iterable(resultBySentences))
    merged = mergeNearbyPairs(flatRes)
    return jsonify(result=merged)"""


@app.route('/check_collocations')
def check_collocations():
    text = request.args.get('text_to_check')
    result = heritageFileProcessor.callProcessorsText(['google'], text, ignorePunct=True)
    return jsonify(result=result)



def mergeNearbyPairs(listOfPairs):
    newListOfPairs = listOfPairs
    prevLastWord = None
    prevPair = None
    for wordPair in listOfPairs:
        wordPairParts = wordPair.split(' ')
        curFirstWord = wordPairParts[0]
        if prevLastWord and prevLastWord==curFirstWord:
            newListOfPairs = [prevPair+' '+wordPairParts[1]]+newListOfPairs
        prevLastWord = wordPairParts[1]
        prevPair = wordPair
    return newListOfPairs

    

if __name__ == "__main__":
    #app.run(host='0.0.0.0', port=2020)
    app.run(host='localhost', port=2020)