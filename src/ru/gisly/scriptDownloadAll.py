# coding=utf-8
__author__ = 'gisly'

import os
import sys

gislyFolder=os.path.dirname(__file__)
ruFolder=os.path.dirname(os.path.abspath(gislyFolder))
parentFolder=os.path.abspath(os.path.dirname(os.path.abspath(ruFolder)))
sys.path.append(parentFolder)
from src.ru.gisly.DownloadGoogleNGramsWithoutDB import GoogleNGramsDownloader


def main():
    if len(sys.argv)<2:
        letterString='abcdefghijklmnopqrstuvwxyz0123456789_'
    else:
        letterString=sys.argv[1]
    d=GoogleNGramsDownloader()
    #d.downloadAllArchives('I:\\heritageData3Grams', 3, letterString, True)
    #d.downloadAllArchives('I:\\heritageData3Grams', 3, letterString)
    d.download('http://storage.googleapis.com/books/ngrams/books/googlebooks-rus-all-3gram-20120701-punctuation.gz','I:\\heritageData3Grams\\punctuation\\')

if __name__ == "__main__":
    main()