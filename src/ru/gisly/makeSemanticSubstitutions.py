# coding=utf-8
__author__ = 'gisly'

from src.ru.gisly.CorpusDb import CorpusDb

from pymorphy import get_morph
import ConfigParser
import os

config = ConfigParser.RawConfigParser()
CONFIG_NAME = 'heritageSettings.ini'
config.read(os.path.join(os.path.dirname(__file__), CONFIG_NAME))


SURNAME_LIST = [u'Иванов', u'Иванова']
FEMININE_NAME_LIST = [u'Анна']
MASCULINE_NAME_LIST = [u'Иван']

GEO_LIST = [u'Москва', u'Россия']

MORPH_DELIM = ','
NOUN_CLASS = u'С'

MASCULINE = u'мр'
FEMININE = u'жр'
NEUTRAL = u'ср'

FEMININE_TYPE = "f"
MASCULINE_TYPE = "m"


host = config.get('DATABASES', 'host')
port = config.getint('DATABASES', 'port')
databaseName = config.get('DATABASES', 'dbname')
corpusDb = CorpusDb(host, port, databaseName)

morph = get_morph('D:\\LingM\\pymorphy\\ru.sqlite-json\\')


def getSemanticSubstitutions(word):
    substSet = set()
    substSet.add(word)
    if isProperName(word):
        lemmata = getLemmata(word)
        if isFeminineName(lemmata):
            substSet = addFeminineNameSubstitutionsToSet(word, substSet)
        if isMasculineName(lemmata):
            substSet = addMasculineNameSubstitutionsToSet(word, substSet)
        if isSurname(lemmata):
            substSet = addSurnameSubstitutionsToSet(word, substSet)
        if isGeo(lemmata):
            substSet = addGeoSubstitutionsToSet(word, substSet)  
    return substSet





def isProperName(word):
    return isCapitalized(word)


def isFeminineName(word):
    return isInDb(existsFeminineName, word)

def isMasculineName(word):
    return isInDb(existsMasculineName, word)

def isSurname(word):
    return isInDb(existsSurname, word)

def isGeo(word):
    return isInDb(existsGeo, word)

def isInDb(predicate, lemmata):
    return any(predicate(word) for word in lemmata)


def existsFeminineName(word):
    return corpusDb.existsName({'name':word, 'type':FEMININE_TYPE})

def existsMasculineName(word):
    return corpusDb.existsName({'name':word, 'type':MASCULINE_TYPE})

def existsSurname(word):
    return corpusDb.existsSurname({'surname':word})

def existsGeo(word):
    return corpusDb.existsGeo({'geo':word})




def addFeminineNameSubstitutionsToSet(word, substSet):
    return addFormsFromList(word, substSet, FEMININE_NAME_LIST, FEMININE)
    
def addMasculineNameSubstitutionsToSet(word, substSet):
    return addFormsFromList(word, substSet, MASCULINE_NAME_LIST, MASCULINE)

def addSurnameSubstitutionsToSet(word, substSet):
    return addFormsFromList(word, substSet, SURNAME_LIST)

def addGeoSubstitutionsToSet(word, substSet):
    return addFormsFromList(word, substSet, GEO_LIST)

def addFormsFromList(word, substSet, substWordList, gender = None):
    forms = getPossibleForms(word)
    for form in forms:
        if isNoun(form) and (gender is None or isGenderCorresponding(form['info'], gender)):
            for subst in substWordList:
                substSet.add(normalizeProperName(createFormForWord(getCase(form['info']), subst)))
                substSet.add(normalizeWord(getCorrespondingAnaphoricPronoun(form['info'], gender)))
    return substSet
    

def createFormForWord(case, word):
    return morph.inflect_ru(word.upper(), case)

def getPossibleForms(word):
    return morph.get_graminfo(word.upper())

def getCase(morphInfo):
    return morphInfo.split(MORPH_DELIM)[-1]

def getLemmata(word):
    allLemmata = morph.normalize(word.upper())
    if type(allLemmata) is set: 
        return allLemmata   
    return [word.upper()]

def isNoun(morphInfo):
    return morphInfo['class']==NOUN_CLASS

def isGenderCorresponding(morphInfo, gender):
    return getGender(morphInfo)==gender

def isCapitalized(word):
    return word[0].isupper()

def normalizeWord(word):
    return word.lower()


def normalizeProperName(proper):
    return proper.lower().capitalize()

def getCorrespondingAnaphoricPronoun(morphInfo, gender=None):
    if not gender:
        gender = getGender(morphInfo)
    if gender == MASCULINE:
        return createFormForWord(getCase(morphInfo), u'ОН')
    elif gender == FEMININE:
        return createFormForWord(getCase(morphInfo), u'ОНА')
    return createFormForWord(getCase(morphInfo), u'ОНО')
    
def getGender(morphInfo):
    return morphInfo.split(MORPH_DELIM)[0]


"""res = getSemanticSubstitutions(u'Климовым')
for element in res:
    print element"""
    
