# coding=utf-8
__author__ = 'gisly'

class QueryWikiNGrams(object):
    def __init__(self, db):
        self.corpusDB = db

    def getJointScore(self, word0, word1):
        return self.corpusDB.getWikiScoreBigram(word0, word1)


