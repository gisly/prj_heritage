# coding=utf-8
__author__ = 'gisly'
import codecs
from src.ru.gisly import lemmer


class QueryGoogleCorpus(object):
    def __init__(self, db):
        self.corpusDB = db

    def processFile(self, filename):
        f=codecs.open(filename,'r', 'utf-8')
        fileText=f.read()
        allWords=lemmer.Tokenizer().tokenizeReturnArray(fileText)
        for i in range(0, len(allWords)-1):
            print self.findRightNeighbours(allWords[i])
            result=self.getJointScore(allWords[i], allWords[i+1])
            print allWords[i], allWords[i+1], result
        f.close()

    def getJointScore(self, word1, word2):
        return self.corpusDB.getScoreNGram(word1, word2)
    
    def getLimit(self, word1, word2):
        return self.corpusDB.countUnigram(word1)* self.corpusDB.countUnigram(word2)

    def findRightNeighbours(self, word1):
        neighbours =  self.corpusDB.getRightNeighbours(word1)
        print len(neighbours)
        for n in neighbours:
            print n

#print QueryGoogleCorpus().getJointScore(u'сравнивается', u'с')