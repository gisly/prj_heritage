#coding:utf8
# coding=utf-8

import codecs, os, re
import nltk
from subprocess import Popen

class Tokenizer:

    def tokenizeWriteNewFile(self,inFile,outFile):
        fIn = codecs.open(inFile, 'r', 'utf-8')
        fOut = codecs.open(outFile, 'w', 'utf-8')
        text = fIn.read()
        fIn.close()
        fOut.write(self.tokenizeText(text))
        fOut.close()

    def tokenizeReturnArray(self, text):
        sents=self.splitTextIntoSentences(text)
        nestedWordList=[nltk.word_tokenize(sent) for sent in sents]
        return nestedWordList
        #return [item for sublist in nestedWordList for item in sublist]
        """splitStr= re.split("(,|\\.|\\?|!|:|»|«|\")*\\s",text)
        return [el for el in splitStr if el is not None and el!='']"""

    def tokenizeText(self, text):
        sents=self.splitTextIntoSentences(text)
        return u'\r\n'.join(u'\r\n'.join(nltk.word_tokenize(sent)) for sent in sents)
        #return u'\r\n'.join(nltk.word_tokenize(text))

    def splitTextIntoSentences(self, text):
        splitter = nltk.data.load('tokenizers/punkt/english.pickle')
        return splitter.tokenize(text)

class Lemmer:
    lemmerProcessingPath='/home/gisly/mystem/lemmer/processing'
    lemmerCleanPath='/home/gisly/mystem/lemmer/texts/clean/'
    lemmerFinalizedPath='/home/gisly/mystem/lemmer/texts/finalized/'
    batFilePath='/do.sh'
    
    
    lemmerProcessingPath='D:/LingM/lemmer/lemmer/processing'
    lemmerCleanPath='D:/LingM/lemmer/lemmer/texts/clean/'
    lemmerFinalizedPath='D:/LingM/lemmer/lemmer/texts/finalized/'
    batFilePath='/do.bat'
    
    
    parserTypeMnemonic="mystem"
    tokenizer=Tokenizer()
    badSymbols={'<':'','>':'','&':'AND'}
    def applyMystem(self,source_dir, to_dir):
        #remove all files from the cleanTexts folder
        self.cleanCleanTextsFolder()
        #copy original files to the lemmers "clean texts" folder
        self.copy_files(source_dir)
        #call mystem
        self.callDoBat()
        #copy the resulting files to the specified folder
        self.copyFinalizedFilesBack(to_dir)
        #remove all files from the cleanTexts folder
        self.cleanCleanTextsFolder()
        
    def tokenizeApplyMystemText(self,text):
        #remove all files from the cleanTexts folder
        self.cleanCleanTextsFolder()
        text = self.normalize(text)
        text=self.tokenize(text)
        #copy original files to the lemmers "clean texts" folder
        #TODO: think!
        newFileName=os.path.basename("temp.txt")
        self.write_file(newFileName,text)
        #call mystem
        self.callDoBat()
        #remove all files from the cleanTexts folder
        text= self.getProcessedFileTextExt(newFileName)
        self.cleanCleanTextsFolder()
        return text

    def tokenizeApplyMystemFile(self,fileName):
        #remove all files from the cleanTexts folder
        self.cleanCleanTextsFolder()
        text=self.getTokenizedText(fileName)
        #copy original files to the lemmers "clean texts" folder
        newFileName=os.path.basename(fileName)
        self.write_file(newFileName,text)
        #call mystem
        self.callDoBat()
        #remove all files from the cleanTexts folder
        text= self.getProcessedFileTextExt(newFileName)
        self.cleanCleanTextsFolder()
        return  text

    def tokenizeApplyMystemFileReturnFile(self,fileName):
        text = self.tokenizeApplyMystemFile(fileName)
        changedName = fileName + '_changed.txt'
        with codecs.open(changedName, 'w', 'utf-8') as fout:
            fout.write(text.replace("windows-1251","utf-8"))
        return changedName
    
    def tokenizeApplyMystemTextReturnFile(self,text):
        text = self.tokenizeApplyMystemText(text)
        changedName = 'temp_changed.txt'
        with codecs.open(changedName, 'w', 'utf-8') as fout:
            fout.write(text.replace("windows-1251","utf-8"))
        return changedName


    def getTokenizedText(self, fileName):
        with codecs.open(fileName, 'r', 'utf-8') as f:
            text=self.tokenize(f.read())
        return text

    def tokenize(self, text):
        return self.tokenizer.tokenizeText(text)

    def applyMystemOneFile(self,fileName,text):
        #remove all files from the cleanTexts folder
        self.cleanCleanTextsFolder()
        #copy original files to the lemmers "clean texts" folder
        self.write_file(fileName,text)
        #call mystem
        self.callDoBat()
        #remove all files from the cleanTexts folder
        text= self.getProcessedFileText(fileName)
        self.cleanCleanTextsFolder()
        return  text

    def cleanCleanTextsFolder(self):
        for the_file in os.listdir(self.lemmerCleanPath):
            file_path = os.path.join(self.lemmerCleanPath, the_file)
            os.unlink(file_path)


    #copy original files to the lemmers "clean texts" folder
    #strip tags, tokenize the files,
    #add headers etc
    def copy_files(self,dir_from):
        tokenizer=Tokenizer()
        for fname in os.listdir(dir_from):
            if not fname.endswith('.txt'):
                continue
            f = codecs.open(dir_from+'\\'+fname, 'r', 'utf-8')
            fOut = codecs.open(self.lemmerCleanPath +'\\'+ fname, 'w', 'utf-8')
            text = f.read()
            f.close()
            #применение токенизатора (файлы могут быть уже токенизированы)
            text=tokenizer.tokenizeText(text)
            fOut.write('<?xml version="1.0" '
                       'encoding="utf-8"?>\r\n'
                       '<html><body>\r\n')
            fOut.write(text)
            fOut.write('</body>\r\n</html>')
            fOut.close()

    def callDoBat(self):
        #из bat-файла убрала команду pause
        do_bat_path = os.path.abspath(self.lemmerProcessingPath)
        os.chdir(do_bat_path)
        p = Popen(do_bat_path + self.batFilePath,
            cwd=do_bat_path, shell=True)
        stdout, stderr = p.communicate()

    def copyFinalizedFilesBack(self, dir_to):
        for fname in os.listdir(self.lemmerFinalizedPath):
            f = codecs.open(self.lemmerFinalizedPath + '\\' + fname, 'r', 'cp1251')
            fOut = codecs.open(dir_to +'\\'+ fname.replace(".xml", ".txt"), 'w', 'utf-8')
            text = f.read()
            f.close()
            fOut.write(text.replace("windows-1251","utf-8"))
            fOut.close()

    def getProcessedFileTextExt(self, fileName):
        return self.getProcessedFileText(fileName.replace('.txt','.xml'))

    def getProcessedFileText(self, fileName):
        f = codecs.open(self.lemmerFinalizedPath + fileName, 'r', 'cp1251')
        return f.read()

    def write_file(self,fname,text):
        fOut = codecs.open(self.lemmerCleanPath +'\\'+ fname, 'w', 'utf-8')
        fOut.write('<?xml version="1.0" '
                   'encoding="utf-8"?>\r\n'
                   '<html><body>\r\n')
        fOut.write(text)
        fOut.write('</body>\r\n</html>')
        fOut.close()


    def normalize(self, text):
        for badSymbol, replacement in self.badSymbols.iteritems():
            text = text.replace(badSymbol, replacement)
        return text






