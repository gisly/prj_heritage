# coding=utf-8
import codecs
import logging
import os
import re
import urllib2
import lxml.html
from src.ru.gisly.heritageDbBuilder import textDB

__author__ = 'Lena'
class ItalkiCrawler(object):




    ITALKI_URL='http://www.italki.com/notebook/russian/all/0/1/'
    ITALKI_ENCODING='utf-8'

    #ITALKI HTML
    ITALKI_TAG_LINK='a'
    ITALKI_TAG_AUTHOR='span'
    ITALKI_TAG_NICKNAME='span'
    ITALKI_TAG_INFO='span'
    ITALKI_TAG_LANGUAGE_LEVEL='em'


    ITALKI_CLASS_LINK='title_txt'
    ITALKI_CLASS_AUTHOR = 'author_txt'
    ITALKI_CLASS_NICKNAME = 'nickname'
    ITALKI_CLASS_INFO = 'des_txt'
    ITALKI_ID_MAIN_TEXT = 'a_NMContent'
    ITALKI_ATTRIB_LANGUAGE_LEVEL = 'title'

    SPEAKS=0
    LEARNS=1
    GENDER_LOCATION=2

    MNEMONIC = 'italki'


    logFile='H:\\heritageLog\downloadItalki.log'
    FORMAT = '%(asctime)-15s %(message)s'
    logging.basicConfig(filename=logFile, level=logging.DEBUG, format=FORMAT)

    dbInstance= textDB.HeritageWebDB()


    EXPORT_FOLDER='H:\\italkiTexts\\'
    METADATA_FILE='_meta.txt'

    def crawlItalki(self, startFrom=0, numOfPages=10):
        self.logToFileConsole(True,True,'started crawling italki with totalPagenum='+str(numOfPages))
        for i in range(startFrom,startFrom+numOfPages):
            self.crawlPage(i)
            if (i%200)==0:
                self.logToFileConsole(True,True,'crawled '+str(i)+' out of '+str(numOfPages))
        self.logToFileConsole(True,True,'ended crawling italki')

    def crawlPage(self, pageNum):
        try:
            self.logToFileConsole(True,True,'started crawling page '+str(pageNum))
            data=self.getHTMLData(self.ITALKI_URL+'/'+str(pageNum), self.ITALKI_ENCODING)
            data_html=lxml.html.document_fromstring(data)
            allLinks = data_html.xpath('//'+self.ITALKI_TAG_LINK+'[@class="'+self.ITALKI_CLASS_LINK+'"]/@href')
            for link in allLinks:
                self.processLink(link)
        except Exception, e:
            self.logToFileConsole(True,True,'error when crawling page '+str(pageNum) + ':' + str(e))
        finally:
            self.logToFileConsole(True,True,'ended crawling page '+str(pageNum))

    def processLink(self, link):
        try:
            self.logToFileConsole(True,True,'started processing link '+link)
            if self.dbInstance.isPostIsInDB(link):
                return
            link_html = self.getHtmlDocument(link,self.ITALKI_ENCODING)

            authorDatetimeBlock = self.getPostAuthorDatetime(link_html)

            authorUrl = self.getPostAuthorUrl(authorDatetimeBlock)
            self.insertAuthorIfNew(authorUrl)

            postText=self.getPostText(link_html)
            print postText

            #todo:
            postDatetime=self.getPostDatetime(authorDatetimeBlock)

            post=dict()
            post['authorId']=authorUrl
            post['raw']=postText
            post['postId']=link
            self.dbInstance.insertPost(post)
        except Exception,e:
            self.logToFileConsole(True,True,'error while parsing '+link+':'+str(e))
            errorLink=dict()
            errorLink['error']=str(e)
            errorLink['link']=link
            self.dbInstance.insertErrorPost(errorLink)
        finally:
            self.logToFileConsole(True,True,'ended processing link '+link)


    def getPostText(self, link_html):
        return link_html.xpath('//*[@id="'+self.ITALKI_ID_MAIN_TEXT+'"]')[0].text_content()

    def getPostAuthorDatetime(self, link_html):
        return self.lxmlXpathDscTagAttr(link_html,self.ITALKI_TAG_AUTHOR,
                                'class',self.ITALKI_CLASS_AUTHOR)[0]\
                                    .xpath('.//li')[0]


    def getPostAuthorUrl(self, authorDatetimeBlock):
        return authorDatetimeBlock.xpath('.//a/@href')[0]

    def insertAuthorIfNew(self, authorUrl):
        if not self.dbInstance.isAuthorInDB(authorUrl):
            author =  self.getAuthorInfo(authorUrl)
            self.dbInstance.insertAuthor(author)

    def getAuthorInfo(self, authorUrl):
        author = dict()
        author['host'] = self.MNEMONIC
        author['authorId']=authorUrl
        author_link_html=self.getHtmlDocument(authorUrl,self.ITALKI_ENCODING)
        authorNickname=self.lxmlXpathDscTagAttr(author_link_html,self.ITALKI_TAG_NICKNAME,
            'class',self.ITALKI_CLASS_NICKNAME)[0].text_content()
        print authorNickname
        author['nickname']=authorNickname
        self.addExtraInfoBlocks(author,author_link_html)
        return author

    def addExtraInfoBlocks(self,author,author_link_html):
        authorInfoBlocks=self.lxmlXpathDscTagAttr(author_link_html,self.ITALKI_TAG_INFO,'class',self.ITALKI_CLASS_INFO)
        speaksNum=  self.SPEAKS
        genderLocation =self.GENDER_LOCATION
        if 'Teaches' in authorInfoBlocks[self.SPEAKS].text_content():
            speaksNum+=1
            genderLocation+=1

        #the first part is the languages the person speaks
        self.addLanguagesSpoken(author,authorInfoBlocks[speaksNum])
        #todo
        #self.getLanguagesLearnt(author,authorInfoBlocks[self.LEARNS])
        self.addGenderLocation(author,authorInfoBlocks[genderLocation])

    def addLanguagesSpoken(self,author,authorLanguagesSpokenBlock):
        langList=authorLanguagesSpokenBlock.text_content().split(':')[1].split(',')
        emBlocks=authorLanguagesSpokenBlock.xpath('.//'+self.ITALKI_TAG_LANGUAGE_LEVEL)
        for lang,emBlock in zip(langList,emBlocks):
            level=emBlock.attrib[self.ITALKI_ATTRIB_LANGUAGE_LEVEL]
            language=lang
            if not level in author:
                author[level] = [language]
            else:
                author[level]+= [language]

    def addGenderLocation(self,author,authorGenderLocationBlock):
        infoList=authorGenderLocationBlock.text_content().split(',')
        for infoData in infoList:
            curInfo=infoData.strip()
            if curInfo == 'Male':
                author['gender']= 0
            elif curInfo == 'Female':
                author['gender']= 1
            elif 'From ' in curInfo:
                curInfo=curInfo.split('From ')[1].strip()
                author['from']=curInfo
            elif 'Living ' in curInfo:
                curInfo=curInfo.split(' in ')[1].split('Map')[0].strip()
                author['living']=curInfo
            else:
                #age
                if re.match('^\d+$',curInfo):
                    author['age']=int(curInfo)


    def getPostDatetime(self, authorDatetimeBlock):
        textC=authorDatetimeBlock.text_content()
        print textC

    def updateWrongAuthors(self):
        self.logToFileConsole(True,True,'started processing authors with no native language data')
        wrongAuthors=self.dbInstance.getAuthorsByQuery({'Native':None})
        for author in wrongAuthors:
            try:
                newAuthorInfo=self.getAuthorInfo(author['authorId'])
                self.dbInstance.updateAuthor(newAuthorInfo)
            except Exception, e:
                print str(e)
        self.logToFileConsole(True,True,'ended processing authors with no native language data')

    def processErrorPostsAgain(self):
        self.logToFileConsole(True,True,'started processing posts which were processed with an error')
        wrongPosts=self.dbInstance.getErrorPosts()
        for post in wrongPosts:
            try:
                errorLink=post['link']
                self.processLink(errorLink)
                self.dbInstance.removeErrorPost(errorLink)
            except Exception, e:
                print str(e)
        self.logToFileConsole(True,True,'ended processing posts which were processed with an error')


    def getHTMLData(self,url,encoding):
        usock = urllib2.urlopen(url)
        data = usock.read()
        usock.close()
        return data.decode(encoding)

    def getHtmlDocument(self,url,encoding):
        link_html=self.getHTMLData(url,self.ITALKI_ENCODING)
        return lxml.html.document_fromstring(link_html)

    def lxmlXpathDscTagAttr(self,html,tagName,attrName, attrValue):
        return html.xpath('.//'+tagName+'[@'+attrName+'="'+attrValue+'"]')


    def exportAllPostsByNotNativeRussians(self, isCyrillicOnly=False):
        authors=self.dbInstance.getAuthorsByQuery({'Native':{'$ne' : 'Russian'}})
        authorCount=0
        for author in authors:
            authorId=author['authorId']
            authorNative=self.getAuthorNative(author)
            arePostsExported=self.exportAllCyrillicPosts(authorId,authorNative,isCyrillicOnly)
            authorCount+=1
            if authorCount % 100 ==0:
                print 'processed '+str(authorCount)+' authors'
            if arePostsExported:
                self.exportMetadata(author)


    def exportAllCyrillicPosts(self, authorId,authorNative, isCyrillicOnly=False):
        try:
            if isCyrillicOnly:
                cyrillicQuery=''
            else:
                cyrillicQuery={'authorId':authorId, 'raw':{'$regex':'.*[АаИиЕеОо].*'}}
            colPosts=self.dbInstance.getPostsByQuery(cyrillicQuery)
            arePostsFound=False
            for post in colPosts:
                arePostsFound=True
                self.exportText(authorId,authorNative,post['postId'],post['raw'])
            return arePostsFound
        except Exception, e:
            print('error in exportAllCyrillicPosts:'+authorId+':'+str(e))

    def exportText(self, authorId,authorNative,postId, postText):
        try:
            authorIdName='italki'+self.getHTTPLinkLastPart(authorId)
            postIdName=self.getHTTPLinkLastPart(postId)
            folderName=self.EXPORT_FOLDER+authorNative+'\\'+authorIdName
            if not os.path.exists(folderName):
                os.makedirs(folderName)
            with codecs.open(folderName+'\\'+postIdName+'.txt', 'w', 'utf-8') as fout:
                fout.write(postText)
        except Exception, e:
            print(authorId+':'+str(e))

    def exportMetadata(self, authorInfo):
        try:
            authorIdName='italki'+self.getHTTPLinkLastPart(authorInfo['authorId'])
            authorNative=self.getAuthorNative(authorInfo)
            folderName=self.EXPORT_FOLDER+authorNative+'\\'+authorIdName
            with codecs.open(folderName+'\\'+self.METADATA_FILE, 'w', 'utf-8') as fout:
                fout.write(self.getAuthorMetadata(authorInfo))
        except Exception, e:
            print(str(e))

    def getAuthorMetadata(self, authorInfo):
        try:
            if 'nickname' in authorInfo:
                nickname=authorInfo['nickname']
            else:
                nickname='NA'
            if 'gender' in authorInfo:
                if authorInfo['gender']==0:
                    gender='M'
                else:
                    gender='F'
            else:
                gender='NA'
            if 'age' in authorInfo:
                age=str(authorInfo['age'])
            else:
                age='NA'
            if 'living' in authorInfo:
                living=authorInfo['living']
            else:
                living='NA'
            Native=self.getAuthorNative(authorInfo)
            return 'nickname:'+nickname+'\r\n'+\
                   'gender:'+gender+'\r\n'+\
                   'age:'+age+'\r\n'+\
                   'living:'+living+'\r\n'+\
                   'Native:'+Native+'\r\n'
        except Exception, e:
            print('error in getAuthorMetadata:'+str(e))

    def getAuthorNative(self, authorInfo):
        if 'Native' in authorInfo:
            Native=authorInfo['Native']
            if isinstance(Native,list):
                NativeRes=''
                for language in Native:
                    NativeRes+='_'+language.strip()
            else:
                NativeRes='_'+Native.strip()
        else:
            NativeRes='NA'
        return NativeRes


    def getHTTPLinkLastPart(self, httpLink):
        return httpLink.split('/')[-1].split('.')[0]

    def logToFileConsole(self, isLogToConsole, isLogToFile, message):
        if isLogToConsole:
            print message
        if isLogToFile:
            logging.debug(message)



crawler=ItalkiCrawler()
#crawler.crawlItalki(0,10)
#crawler.updateWrongAuthors()
crawler.exportAllPostsByNotNativeRussians()
#crawler.exportAllCyrillicPosts('http://www.italki.com/T009820545.htm','aaaa', False)