# coding=utf-8
import pymongo

__author__ = 'Lena'
class HeritageWebDB:
    def __init__(self,host='localhost',port=27017,dbName='heritageWebDB'):
        self.connection=pymongo.Connection(host,port)
        self.db=self.connection[dbName]
    def __del__(self):
        self.connection.close()

    def insertPost(self,post):
        colPosts=self.db.posts
        colPosts.insert(post)

    def insertAuthor(self,author):
        colAuthors=self.db.authors
        colAuthors.insert(author)

    def insertErrorPost(self, errorLink):
        colErrorLinks=self.db.errorLinks
        colErrorLinks.insert(errorLink)

    def getErrorPosts(self):
        colErrorLinks=self.db.errorLinks
        return colErrorLinks.find()

    def removeErrorPost(self,errorLink):
        colErrorLinks=self.db.errorLinks
        colErrorLinks.remove({'link':errorLink})

    def isAuthorInDB(self, authorId):
        colAuthors=self.db.authors.find({'authorId':authorId})
        return colAuthors.count() > 0

    def isPostIsInDB(self, postId):
        colPosts=self.db.posts.find({'postId':postId})
        return colPosts.count() > 0

    def getAuthorsByQuery(self,query):
        return self.db.authors.find(query)

    def getAuthorById(self,authorId):
        return self.db.authors.find({'authorId':authorId})



    def updateAuthor(self, authorInfo):
        colAuthors=self.db.authors
        colAuthors.update({'authorId':authorInfo['authorId']},authorInfo)


    def getPostsByQuery(self, query):
        return self.db.posts.find(query)


